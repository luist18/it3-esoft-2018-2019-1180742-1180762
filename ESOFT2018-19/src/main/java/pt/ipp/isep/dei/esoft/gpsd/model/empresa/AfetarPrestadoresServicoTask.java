package pt.ipp.isep.dei.esoft.gpsd.model.empresa;

import pt.ipp.isep.dei.esoft.gpsd.controller.AfetarPrestadoresServicoController;

import java.util.TimerTask;

/**
 * Task para a afetação de prestadores de serviço automática.
 *
 * @author Márcio Duarte
 * @author Luís Tavares
 */
public class AfetarPrestadoresServicoTask extends TimerTask {

	/**
	 * Corre a task.
	 */
	@Override
	public void run() {
		AfetarPrestadoresServicoController controller = new AfetarPrestadoresServicoController();
		controller.afetarPrestadoresServico();
	}
}
