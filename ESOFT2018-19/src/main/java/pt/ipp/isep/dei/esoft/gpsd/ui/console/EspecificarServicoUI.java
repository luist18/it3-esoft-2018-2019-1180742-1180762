/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.ui.console;

import pt.ipp.isep.dei.esoft.gpsd.controller.EspecificarServicoController;
import pt.ipp.isep.dei.esoft.gpsd.model.CategoriaServico;
import pt.ipp.isep.dei.esoft.gpsd.model.servico.TipoServico;
import pt.ipp.isep.dei.esoft.gpsd.ui.console.utils.Utils;

public class EspecificarServicoUI {

    private EspecificarServicoController controller;

    public EspecificarServicoUI() {
        controller = new EspecificarServicoController();
    }

    public void run() {
        System.out.println("\nEspecificar Serviço:");

        boolean confirmacao = false;
        while (!confirmacao) {
            if (introduzDados()) {
                if (controller.valida()) {
                    confirmacao = confirma();
                    if (confirmacao) {
                        if (controller.registaServico()) {
                            System.out.printf("Serviço registado com sucesso.");
                        } else {
                            System.out.println("Ocorreu um erro.");
                        }
                    } else {
                        System.out.println("Especificação de serviço cancelada.");
                        break;
                    }
                } else {
                    System.out.println("Ocorreu um erro.");
                }
            } else {
                System.out.println("Ocorreu um erro.");
            }
        }
    }

    private boolean introduzDados() {
        TipoServico ts = lerTipoServico();
        String id = Utils.readLineFromConsole("ID do serviço: ");
        String descB = Utils.readLineFromConsole("Descrição breve do serviço: ");
        String descC = Utils.readLineFromConsole("Descrição completa do serviço: ");
        double custo = Utils.readDoubleFromConsole("Custo do serviço: ");
        String catId = lerCategoria();

        if (controller.novoServico(id, descB, descC, custo, catId, ts)) {
            if (controller.getServico().possuiOutrosAtributos()) {
                double duracaoPre = Utils.readDoubleFromConsole("Duração pré-determinada: ");
                controller.getServico().setOutrosAtributos(duracaoPre);
            }
            return true;
        } else {
            return false;
        }
    }

    private void apresentaTiposServico() {
        System.out.println("Tipos de serviço:");
        for (TipoServico ts : TipoServico.values()) {
            System.out.printf("%d - %s", ts.ordinal() + 1, ts.toString());
        }
    }

    private void apresentaCategorias() {
        System.out.println("Categorias:");
        for (CategoriaServico cat : controller.getCategorias()) {
            System.out.printf("%d - %s", Integer.parseInt(cat.getCodigo()) + 1, cat.toString());
        }
    }


    private TipoServico lerTipoServico() {
        apresentaTiposServico();
        String strId = Utils.readLineFromConsole("Tipo de serviço: ");
        try {
            int id = Integer.parseInt(strId);
            return TipoServico.values()[--id];
        } catch (Exception e) {
            return null;
        }
    }

    private String lerCategoria() {
        apresentaCategorias();
        return Utils.readLineFromConsole("Categoria: ");
    }

    private void apresentaServico() {
        System.out.println(controller.getServico().toString());
    }

    private boolean confirma() {
        apresentaServico();
        String confirmacao = Utils.readLineFromConsole("Pretende registar o serviço? (Y/N)");
        if (confirmacao.equalsIgnoreCase("y")) {
            return true;
        } else {
            return false;
        }
    }
}
