package pt.ipp.isep.dei.esoft.gpsd.model.empresa;

public interface ServicoExternoEmail {

	/**
	 * Deve permitir a possibilidade de enviar um email.
	 *
	 * @param endereco endereço para o qual enviar.
	 * @param mensagem mensagem a enviar.
	 */
	void enviarEmail(String endereco, String mensagem);
}
