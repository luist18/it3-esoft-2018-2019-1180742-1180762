package pt.ipp.isep.dei.esoft.gpsd.model.prestador;

import pt.ipp.isep.dei.esoft.gpsd.model.AreaGeografica;
import pt.ipp.isep.dei.esoft.gpsd.model.CategoriaServico;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe representativa de um prestador de serviço.
 *
 * @author Márcio Duarte
 * @author Luís Tavares
 */
public class PrestadorServico {

	/**
	 * Numero mecanográfico do prestador.
	 */
	private String strNumero;

	/**
	 * Nome completo do prestador.
	 */
	private String nomeCompleto;

	/**
	 * Nome abreviado do prestador.
	 */
	private String nomeAbreviado;

	/**
	 * Endereço de correio eletrónico do prestador.
	 */
	private String email;

	/**
	 * Cateogiras ao qual o prestador se dispõe.
	 */
	private List<CategoriaServico> categoriasServico = new ArrayList<>();

	/**
	 * Áreas geográficas onde o prestador se insere e trabalha.
	 */
	private List<AreaGeografica> areasGeograficas = new ArrayList<>();


	/**
	 * Lista de disponibilidade diária do prestador.
	 */
	private ListaDisponibilidadeDiaria listaDisponibilidadeDiaria;

	/**
	 * Construtor de prestador de serviço que inicializa o numero meacnográfico, o nome completo e abreviado e o email.
	 *
	 * @param strNumero     numero mecanográfico do prestador.
	 * @param nomeCompleto  nome completo do prestador.
	 * @param nomeAbreviado nome abreviado do prestador.
	 * @param email         email institucional do prestador.
	 */
	public PrestadorServico(String strNumero, String nomeCompleto, String nomeAbreviado, String email) {
		this.strNumero = strNumero;
		this.nomeCompleto = nomeCompleto;
		this.nomeAbreviado = nomeAbreviado;
		this.email = email;
		this.listaDisponibilidadeDiaria = new ListaDisponibilidadeDiaria();
	}

	/**
	 * Verifica as verficações locais de um prestador de serviço.
	 *
	 * @return se, localmente, o prestador de serviço é válido.
	 */
	public boolean validarDadosLocais() {
		boolean valido = true;

		//valida se o número mecanográfico é um número, de facto.
		try {
			Integer.parseInt(strNumero);
		} catch (Exception e) {
			valido = false;
		}

		if (!validarEmail())
			valido = false;

		return valido;
	}

	/**
	 * Adiciona uma área geográfica.
	 *
	 * @param areaGeografica área geográfica a adicionar.
	 * @return se a área foi adicionada corretamente.
	 */
	public boolean addAreaGeografica(AreaGeografica areaGeografica) {
		if (!areasGeograficas.contains(areaGeografica))
			return areasGeograficas.add(areaGeografica);
		else
			return false;
	}

	/**
	 * Adiciona uma categoria de serviço às existentes.
	 *
	 * @param categoriaServico categoria de serviço a adicionar.
	 * @return se a categoria foi adicionada corretamente.
	 */
	public boolean addCategoria(CategoriaServico categoriaServico) {
		return categoriasServico.add(categoriaServico);
	}

	/**
	 * Carrega a lista de categorias de um prestador de serviço, copiando outra. I.e. por composição.
	 *
	 * @param categorias lista de categorias a carregar.
	 */
	public void carregarListaCategorias(List<CategoriaServico> categorias) {
		this.categoriasServico = new ArrayList<>(categorias);
	}

	/**
	 * Carrega a lista de áreas geográficas de um prestador de serviço, copiando outra. I.e. por composição.
	 *
	 * @param areasGeograficas lista de áreas geográficas a carregar.
	 */
	public void carregarListaAreasGeograficas(List<AreaGeografica> areasGeograficas) {
		this.areasGeograficas = new ArrayList<>(areasGeograficas);
	}

	/**
	 * Verifica se um prestador tem alguma disponibilidade adiante.
	 *
	 * @return se o prestador tem disponiblidade.
	 */
	public boolean temDisponibilidade() {
		return listaDisponibilidadeDiaria.isDisponivel();
	}

	/**
	 * Retorna o nome abreviado do prestador.
	 *
	 * @return o nome abreviado do prestador.
	 */
	public String getNomeAbreviado() {
		return nomeAbreviado;
	}

	/**
	 * Retorna o email do prestador.
	 *
	 * @return o email do prestador.
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Devolve uma string com a descrição do prestador.
	 *
	 * @return uma string com a descrição do prestador.
	 */
	@Override
	public String toString() {
		return "PrestadorServico\n" +
				"\n\tNúmero mecanográfico = '" + strNumero + '\'' +
				"\n\t, Nome completo ='" + nomeCompleto + '\'' +
				"\n\t, Nome abreviado ='" + nomeAbreviado + '\'' +
				"\n\t, Email ='" + email + '\'' + "\n";
	}

	/**
	 * Valida se um email é válido ou não.
	 *
	 * @return se o email é válido ou não.
	 */
	private boolean validarEmail() {
		String[] emailArr = email.split("@");
		if (emailArr.length != 2)
			return false;
		return emailArr[1].contains(".");//adicionar mais condições aqui
	}

	/**
	 * Retorna a lista de disponibilidade diária.
	 *
	 * @return a lista de disponibilidaed diária.
	 */
	public ListaDisponibilidadeDiaria getListaDisponibilidadeDiaria() {
		return listaDisponibilidadeDiaria;
	}

	public List<AreaGeografica> getAreasGeograficas() {
		return areasGeograficas;
	}

	public List<CategoriaServico> getCategoriasServico() {
		return categoriasServico;
	}
}
