package pt.ipp.isep.dei.esoft.gpsd.controller;

import pt.ipp.isep.dei.esoft.autorizacao.model.RegistoNotificacoes;
import pt.ipp.isep.dei.esoft.gpsd.model.Afetacao;
import pt.ipp.isep.dei.esoft.gpsd.model.Notificavel;
import pt.ipp.isep.dei.esoft.gpsd.model.PedidoPrestacaoServico;
import pt.ipp.isep.dei.esoft.gpsd.model.cliente.Cliente;
import pt.ipp.isep.dei.esoft.gpsd.model.empresa.Empresa;
import pt.ipp.isep.dei.esoft.gpsd.model.empresa.RegistoAfetacoes;
import pt.ipp.isep.dei.esoft.gpsd.model.empresa.RegistoOrdensExecucaoServico;
import pt.ipp.isep.dei.esoft.gpsd.model.empresa.ServicoExternoEmail;
import pt.ipp.isep.dei.esoft.gpsd.ui.console.DecidirSobrePeriodoPropostoUI;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Classe que controla UC de decidir sobre o período proposto para a realização de um serviço.
 *
 * @author Márcio Duarte
 * @author Luís Tavares
 */
public class DecidirSobrePeriodoPropostoController implements Notificavel {

	/**
	 * Empresa em vigor.
	 */
	private Empresa empresa;

	/**
	 * Pedido sobre o qual se vai decidir o período proposto.
	 */
	private PedidoPrestacaoServico pedidoPrestacaoServico;

	/**
	 * Coleção com todas as afetações do pedido de prestação.
	 */
	private Set<Afetacao> afetacoes;

	/**
	 * Contrutor vazio.
	 */
	public DecidirSobrePeriodoPropostoController() {
		this.empresa = AplicacaoGPSD.getInstance().getEmpresa();
	}

	/**
	 * Notifica um utilizador que um dos seus pedidos foo afetado.
	 *
	 * @param pedidoPrestacaoServico pedido afetado.
	 */
	public void notificarUtilizador(PedidoPrestacaoServico pedidoPrestacaoServico) {
		this.pedidoPrestacaoServico = pedidoPrestacaoServico;
		ServicoExternoEmail servicoEmail = empresa.getServicoExternoEmail();
		Cliente cliente = pedidoPrestacaoServico.getCliente();

		String notificacao = escreverNotificacaoPedidoPrestacao(pedidoPrestacaoServico);
		servicoEmail.enviarEmail(cliente.getEmail(), notificacao);

		RegistoNotificacoes registoNotificacoes = empresa.getAutorizacaoFacade().getRegistoNotificacoes();
		registoNotificacoes.criarNotificacao(cliente.getEmail(), this);
	}

	public void runNotificao() {
		DecidirSobrePeriodoPropostoUI ui = new DecidirSobrePeriodoPropostoUI(this);
		ui.run();
	}

	/**
	 * Retorna uma lista de horarios para o pedido de prestação.
	 *
	 * @return uma lista de horarios.
	 */
	public List<Date> getHorarios() {
		RegistoAfetacoes registoAfetacoes = empresa.getRegistoAfetacoes();
		this.afetacoes = registoAfetacoes.getAfetacoesDePedido(pedidoPrestacaoServico);

		List<Date> horarios = new ArrayList<>();
		for (Afetacao afetacao : afetacoes)
			horarios.add(afetacao.getHorario());

		return horarios;
	}

	/**
	 * Cria as ordens de execuçao de serviço referentes a cada uma das afetações.
	 */
	public void criarOrdensExecucaoServico() {
		RegistoOrdensExecucaoServico registoOrdens = empresa.getRegistoOrdensExecucaoServico();

		for (Afetacao afetacao : afetacoes) {
			afetacao.confirmar();
			registoOrdens.criarOrdensExecucaoDeServico(afetacao);
		}
	}

	/**
	 * Elimina as afetações do registo de afetaçoes para que possam voltar a ser escalonadas.
	 */
	public void eliminarAfetacoes() {
		RegistoAfetacoes registoAfetacoes = empresa.getRegistoAfetacoes();

		for (Afetacao afetacao : afetacoes)
			registoAfetacoes.eliminarAfetacao(afetacao);

	}

	/**
	 * Escreve uma mensagem de notificação sobre o pedido de prestação.
	 *
	 * @param pedidoPrestacaoServico pedido sde prestação.
	 * @return a mensagem de notificação.
	 */
	private String escreverNotificacaoPedidoPrestacao(PedidoPrestacaoServico pedidoPrestacaoServico) {
		return String.format("O pedido de prestação %s, foi afetado.\nAceda ao sistema para verificar e confirmar!", pedidoPrestacaoServico.toString());
	}
}
