package pt.ipp.isep.dei.esoft.gpsd.ui.console;

import pt.ipp.isep.dei.esoft.gpsd.controller.DecidirSobrePeriodoPropostoController;
import pt.ipp.isep.dei.esoft.gpsd.ui.console.utils.Utils;

import java.util.Date;
import java.util.List;

/**
 * UI para o UC sobre decisão sobre horários de um pedido de prestação de serviço.
 *
 * @author Márcio Duarte
 * @author Luís Tavares
 */
public class DecidirSobrePeriodoPropostoUI {

	/**
	 * Controller do UC.
	 */
	DecidirSobrePeriodoPropostoController controller;

	/**
	 * Construtor que inicia o controller.
	 *
	 * @param controller controller do UC.
	 */
	public DecidirSobrePeriodoPropostoUI(DecidirSobrePeriodoPropostoController controller) {
		this.controller = controller;
	}

	/**
	 * Método que corre a UI.
	 */
	public void run() {
		if (concordaComOsHorarios())
			controller.criarOrdensExecucaoServico();
		else
			controller.eliminarAfetacoes();

		notificarUtilizador();
	}


	/**
	 * Verifica se o utilizador concorda com os horários estipulados.
	 *
	 * @return se o utilizador concorda com os horários estipulados.
	 */
	private boolean concordaComOsHorarios() {
		List<Date> horarios = controller.getHorarios();
		Utils.apresentaLista(horarios, "Aqui estãos os horários afetados do seu pedido de prestação.");

		return Utils.confirma("Concorda com os horários estipulados?");
	}

	/**
	 * Notifica o utilizador que o UC está terminado.
	 */
	private void notificarUtilizador() {
		System.out.println("Operação concluída com sucesso.");
	}
}
