package pt.ipp.isep.dei.esoft.gpsd.model.empresa;

import pt.ipp.isep.dei.esoft.gpsd.model.Exportador;

import java.util.ArrayList;

public class RegistoExportadores {

    private final ArrayList<Exportador> exportadores;

    public RegistoExportadores() {
        this.exportadores = new ArrayList<>();
    }

    public Exportador getExpotadorByFormato(String formato) {
        Exportador exportador = null;
        for (Exportador e : exportadores) {
            if (e.getFormato().equalsIgnoreCase(formato)) {
                exportador = e;
                break;
            }
        }
        return exportador;
    }

}
