package pt.ipp.isep.dei.esoft.gpsd.model.cliente;

import pt.ipp.isep.dei.esoft.gpsd.model.EnderecoPostal;

import java.util.ArrayList;
import java.util.List;

/**
 * Lista de endereços postais de um utilizador.
 *
 * @author Márcio Duarte
 * @author Luís Tavares
 */
public class ListaEnderecosPostais {

	private List<EnderecoPostal> listaEnderecosPostais = new ArrayList<>();

	public ListaEnderecosPostais() {

	}

	/**
	 * Perimite a criação de um endereço postal de forma estática.
	 *
	 * @param strLocal      local do endereço postal.
	 * @param strCodPostal  codigo postal do endereço postal.
	 * @param strLocalidade localidade do endereço postal.
	 * @return o endereço postal criado.
	 */
	public static EnderecoPostal novoEnderecoPostal(String strLocal, String strCodPostal, String strLocalidade) {
		return new EnderecoPostal(strLocal, strCodPostal, strLocalidade);
	}

	/**
	 * Regista um endereço postal na lista.
	 *
	 * @param enderecoPostal endereço postal a ser registado.
	 * @return se o endereço foi corretamente registado.
	 */
	public boolean registarEndereco(EnderecoPostal enderecoPostal) {
		//se o endereço não for válido, retorna imediatamente falso.
		if (!validarEndereco(enderecoPostal)) return false;

		return addEndereco(enderecoPostal);
	}

	/**
	 * Cria um endereço postal.
	 *
	 * @param endereco     endereço ou local do endereço postal.
	 * @param codPostalStr codigo postal do endereço postal.
	 * @param localidade   localidade do endereço postal.
	 * @return o EndereçoPostal criado.
	 */
	public EnderecoPostal criarEndereco(String endereco, String codPostalStr, String localidade) {
		return new EnderecoPostal(endereco, codPostalStr, localidade);
	}

	/**
	 * Verifica se um endereço é válido ou não.
	 *
	 * @param enderecoPostal endereço a validar.
	 * @return se o endereço é válido.
	 */
	public boolean validarEndereco(EnderecoPostal enderecoPostal) {
		return enderecoPostal.validarDadosLocais() && validarDadosGlobais(enderecoPostal);
	}

	/**
	 * Verifica se um endereço é válido no ponto de vista geral da empresa.
	 *
	 * @param enderecoPostal endereço postal a validar.
	 * @return se o endereço é válido.
	 */
	private boolean validarDadosGlobais(EnderecoPostal enderecoPostal) {
		//TODO: implement method.
		return !listaEnderecosPostais.contains(enderecoPostal);
	}

	/**
	 * Adiciona um endereço postal à lista.
	 *
	 * @param enderecoPostal endereço postal a adicionar.
	 * @return se o endereço foi adicionado corretamente.
	 */
	public boolean addEndereco(EnderecoPostal enderecoPostal) {
		return listaEnderecosPostais.add(enderecoPostal);
	}

	/**
	 * Remove um endereço postal da lista.
	 *
	 * @param enderecoPostal endereço postal a remover.
	 * @return se o endereço foi corretamente removido.
	 */
	public boolean remove(EnderecoPostal enderecoPostal) {
		return listaEnderecosPostais.remove(enderecoPostal);
	}

	/**
	 * Retorna a listagem dos endereços postais.
	 *
	 * @return a listagemd dos endereços postais.
	 */
	public List<EnderecoPostal> getLista() {
		return new ArrayList<>(listaEnderecosPostais);
	}
}
