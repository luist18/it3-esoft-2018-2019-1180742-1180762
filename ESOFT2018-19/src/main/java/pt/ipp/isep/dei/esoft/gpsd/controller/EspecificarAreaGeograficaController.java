package pt.ipp.isep.dei.esoft.gpsd.controller;

import pt.ipp.isep.dei.esoft.gpsd.model.AreaGeografica;
import pt.ipp.isep.dei.esoft.gpsd.model.Atuacao;
import pt.ipp.isep.dei.esoft.gpsd.model.CodigoPostal;
import pt.ipp.isep.dei.esoft.gpsd.model.Constantes;
import pt.ipp.isep.dei.esoft.gpsd.model.empresa.Empresa;
import pt.ipp.isep.dei.esoft.gpsd.model.empresa.RegistoAreasGeograficas;
import pt.ipp.isep.dei.esoft.gpsd.model.empresa.ServicoExterno;

import java.util.List;

public class EspecificarAreaGeograficaController {

    private Empresa empresa;

    private RegistoAreasGeograficas registoAreasGeograficas;
    private ServicoExterno servExt;

    private AreaGeografica areaGeo;

    public EspecificarAreaGeograficaController() {
        if (!AplicacaoGPSD.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_ADMINISTRATIVO))
            throw new IllegalStateException("Utilizador não Autorizado");
        this.empresa = AplicacaoGPSD.getInstance().getEmpresa();
        iniciar();
    }

    public void iniciar() {
        this.registoAreasGeograficas = empresa.getRegistoAreasGeograficas();
        this.servExt = empresa.getServicoExterno();
    }

    public boolean novaAreaGeografica(String designacao, double custo, CodigoPostal codigoBase, double raio) {
        List<Atuacao> listaAtuacao = servExt.getListaAtuacao(codigoBase, raio);
        if (listaAtuacao != null) {
            areaGeo = registoAreasGeograficas.novaAreaGeografica(designacao, custo, codigoBase, raio, listaAtuacao);
            return true;
        } else {
            return false;
        }
    }

    public boolean valida() {
        return registoAreasGeograficas.validar(areaGeo);
    }

    public boolean registaAreaGeografica() {
        return registoAreasGeograficas.registaAreaGeografica(areaGeo);
    }

    public AreaGeografica getAreaGeo() {
        return areaGeo;
    }
}
