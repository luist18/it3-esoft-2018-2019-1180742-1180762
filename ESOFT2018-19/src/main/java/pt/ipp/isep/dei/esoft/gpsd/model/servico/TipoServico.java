package pt.ipp.isep.dei.esoft.gpsd.model.servico;

public enum TipoServico {
    FIXO {
        @Override
        public String toString() {
            return "Fixo";
        }
    }, EXPANSIVEL {
        @Override
        public String toString() {
            return "Expansível";
        }
    }, LIMITADO {
        @Override
        public String toString() {
            return "Limitado";
        }
    }
}
