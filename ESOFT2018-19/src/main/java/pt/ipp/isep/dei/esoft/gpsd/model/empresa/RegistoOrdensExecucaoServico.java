package pt.ipp.isep.dei.esoft.gpsd.model.empresa;

import pt.ipp.isep.dei.esoft.gpsd.model.Afetacao;
import pt.ipp.isep.dei.esoft.gpsd.model.DescricaoServico;
import pt.ipp.isep.dei.esoft.gpsd.model.OrdemExecucaoServico;
import pt.ipp.isep.dei.esoft.gpsd.model.PedidoPrestacaoServico;
import pt.ipp.isep.dei.esoft.gpsd.model.prestador.PrestadorServico;

import java.util.*;

/**
 * Instancia que alberga todas as ordens de execução de serviço.
 *
 * @author Márcio Duarte
 * @author Luís Tavares
 */
public class RegistoOrdensExecucaoServico {

    /**
     * Coleção com todas as ordens de execução de serviço.
     */
    private Set<OrdemExecucaoServico> ordensExecucaoServico = new HashSet<>();

    /**
     * Cria uma ordem de execução de serviço a partir de uma afetação.
     *
     * @param afetacao afetação sob a qual se baseia.
     */
    public void criarOrdensExecucaoDeServico(Afetacao afetacao) {
        PrestadorServico prestador = afetacao.getPrestadorServico();
        PedidoPrestacaoServico pedidoPrestacaoServico = afetacao.getPedidoPrestacaoServico();
        DescricaoServico descricaoServico = afetacao.getDescricaoServico();
        Date horario = afetacao.getHorario();

        OrdemExecucaoServico ordem = new OrdemExecucaoServico(prestador, pedidoPrestacaoServico, descricaoServico, horario, "Por fazer");
        this.ordensExecucaoServico.add(ordem);
    }

    public List<OrdemExecucaoServico> consultar(String estado, PrestadorServico prestadorServico) {
        if (!validarEstado(estado)) return null;
        List<OrdemExecucaoServico> consulta = new ArrayList<>();
        for (OrdemExecucaoServico ordemExecucaoServico : ordensExecucaoServico) {
            if (ordemExecucaoServico.getEstado().equalsIgnoreCase(estado)
                    && ordemExecucaoServico.getPrestadorServico().equals(prestadorServico))
                consulta.add(ordemExecucaoServico);
        }
        return consulta;
    }

    public boolean validarEstado(String estado){
        return true;
    }

}
