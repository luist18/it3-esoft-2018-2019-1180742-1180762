package pt.ipp.isep.dei.esoft.gpsd.model.empresa;

import pt.ipp.isep.dei.esoft.autorizacao.AutorizacaoFacade;
import pt.ipp.isep.dei.esoft.gpsd.model.prestador.PrestadorServico;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe responsável pela gestão de prestadores de serviço.
 *
 * @author Márcio Duarte
 * @author Luís Tavares
 */
public class RegistoPrestadoresServico {

	/**
	 * Lista com todos os prestadores de serviço.
	 */
	private List<PrestadorServico> prestadoresServico = new ArrayList<>();

	/**
	 * Empresa onde o prestador de serviço se está a registar.
	 */
	private Empresa empresa;

	/**
	 * Contructor que recebe a empresa;
	 */
	public RegistoPrestadoresServico(Empresa empresa) {
		this.empresa = empresa;
	}

	/**
	 * Cria uma instância de prestador de serviço.
	 *
	 * @param strNumero     número mecanográfico atribuído ao prestador.
	 * @param nomeCompleto  nome completo do prestador.
	 * @param nomeAbreviado nome abreviado do prestador.
	 * @param email         email do prestador.
	 * @return a instãncia de prestador criada.
	 */
	public PrestadorServico criarPrestadorServico(String strNumero, String nomeCompleto, String nomeAbreviado, String email) {
		return new PrestadorServico(strNumero, nomeCompleto, nomeAbreviado, email);
	}

	/**
	 * Cria uma palavra passe aleatória.
	 *
	 * @return a palavra passe criada.
	 */
	public String gerarPalavraPasse() {
		final int numeroDeCaracteres = 8;

		int random;
		String palavraPasse = "";

		for (int car = 0; car < numeroDeCaracteres; car++) {
			//generates a random number, letter or symbol.
			random = (int) Math.random() * 74 + 48;
			palavraPasse += (char) random;
		}

		return palavraPasse;
	}

	/**
	 * Verifica se um prestador de serviço é válido.
	 *
	 * @param prestadorServico prestador de serviço a verificar.
	 * @return se o prestador é, de facto, válido.
	 */
	public boolean validarPrestadorServico(PrestadorServico prestadorServico) {
		return prestadorServico.validarDadosLocais() && validarDadosGlobaisPrestadorServico(prestadorServico);
	}

	/**
	 * Regista um prestador de serviço no sistema.
	 *
	 * @param prestadorServico prestador de serviço a registar.
	 * @param password palavra-passe do prestador de serviço.
	 */
	public void registarPrestadorServico(PrestadorServico prestadorServico, String password) {
		String nome = prestadorServico.getNomeAbreviado();
		String email = prestadorServico.getEmail();
		AutorizacaoFacade facade = empresa.getAutorizacaoFacade();
		facade.registaUtilizadorComPapel(nome, email, password, "PRESTADOR");
	}

	/**
	 * Procura o prestador que tenha o referido email.
	 *
	 * @param email email a verificar.
	 * @return o prestador que tem o email, ou null caso nenhum o tenha.
	 */
	public PrestadorServico getPrestadorByEmail(String email) {
		for (PrestadorServico prestadorServico : prestadoresServico) {
			if (prestadorServico.getEmail().equals(email))
				return prestadorServico;
		}
		return null;
	}

	/**
	 * Procura prestadores, verificando se estão disponíveis adiante, retornando assim uma lista com
	 * todos os prestadores que têm disponibilidade.
	 *
	 * @return uma lista com todos os prestadores de serviço que têm disponibiliade.
	 */
	public List<PrestadorServico> getPrestadoresComDisponibilidade() {
		List<PrestadorServico> prestadoresComDisponibildade = new ArrayList<>();

		for(PrestadorServico prestador : prestadoresServico)
			if(prestador.temDisponibilidade())
				prestadoresComDisponibildade.add(prestador);

		return prestadoresComDisponibildade;
	}

	/**
	 * Valida os dados de um prestador de serviço de um ponto de vista global (i.e. duplicados).
	 *
	 * @param prestadorServico prestador de serviço a verificar.
	 * @return se o prestador é valido globalmente ou não.
	 */
	private boolean validarDadosGlobaisPrestadorServico(PrestadorServico prestadorServico) {
		return !prestadoresServico.contains(prestadorServico);
	}

}
