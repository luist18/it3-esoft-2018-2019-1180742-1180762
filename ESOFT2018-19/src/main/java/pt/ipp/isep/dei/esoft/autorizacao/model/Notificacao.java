package pt.ipp.isep.dei.esoft.autorizacao.model;

import pt.ipp.isep.dei.esoft.gpsd.model.Notificavel;

/**
 * Notificação com o propósito de redirecionar o utilizador para um UC aquando do início de sessão.
 *
 * @author Márcio Duarte
 * @author Luís Tavares
 */
public class Notificacao {

	/**
	 * Email do utilizador ao qual a notificação é associada.
	 */
	private String emailDoUtilizador;

	/**
	 * Controller para onde o utilizador será redirecionado.
	 */
	private Notificavel ucController;

	/**
	 * Construtor com todos os parâmetros.
	 *
	 * @param emailDoUtilizador email do utilzador ao qual a notificação é associada.
	 * @param ucController      controller para onde o utilizador é redirecionado.
	 */
	public Notificacao(String emailDoUtilizador, Notificavel ucController) {
		this.emailDoUtilizador = emailDoUtilizador;
		this.ucController = ucController;
	}

	/**
	 * Verifica se a notifcação é de um dado utilizador.
	 *
	 * @param utilizador utilizador a verificar.
	 * @return se a notificação é do utilizador.
	 */
	public boolean isDoUtilizador(Utilizador utilizador) {
		return this.emailDoUtilizador.equals(utilizador.getEmail());
	}

	/**
	 * Verifica se a notifcação é de um dado utilizador.
	 *
	 * @param emailDoUtilizador email do utilizador a verificar.
	 * @return se a notificação é do utilizador.
	 */
	public boolean isDoUtilizador(String emailDoUtilizador) {
		return this.emailDoUtilizador.equals(emailDoUtilizador);
	}

	/**
	 * Retorna o UC controller da notificacao.
	 *
	 * @return o uc controller da notificacao.
	 */
	public Notificavel getUcController() {
		return ucController;
	}
}
