package pt.ipp.isep.dei.esoft.gpsd.model;

/**
 * Refere-se a preferencia de horário com data, hora e prioridade.
 */
public class PreferenciaHorario {

	/**
	 * Dia do horário.
	 */
	private String strData;

	/**
	 * Hora do horário.
	 */
	private String strHora;

	/**
	 * Prioridade do horário por ordem inversa.
	 */
	private int prioridade;

	/**
	 * Construtor com todos os parametros.
	 *
	 * @param strData    dia.
	 * @param strHora    hora.
	 * @param prioridade prioridade por ordem inversa.
	 */
	public PreferenciaHorario(String strData, String strHora, int prioridade) {
		this.strData = strData;
		this.strHora = strHora;
		this.prioridade = prioridade;
	}
}
