package pt.ipp.isep.dei.esoft.gpsd.model.empresa;

import pt.ipp.isep.dei.esoft.gpsd.model.Afetacao;
import pt.ipp.isep.dei.esoft.gpsd.model.DescricaoServico;
import pt.ipp.isep.dei.esoft.gpsd.model.PedidoPrestacaoServico;
import pt.ipp.isep.dei.esoft.gpsd.model.prestador.PrestadorServico;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Classe responsável por manter as afetações.
 *
 * @author Márcio Duarte
 * @author Luís Tavares
 */
public class RegistoAfetacoes {

	/**
	 * Todas as afetações em vigor.
	 */
	private Set<Afetacao> afetacoes = new HashSet<>();

	/**
	 * Construtor.
	 */
	public RegistoAfetacoes() {

	}

	/**
	 * Guarda uma afetação segundo os seus parametros.
	 *
	 * @param pedidoPrestacao  pedido de prestação correspondente.
	 * @param servico          descrição de serviço correspondente.
	 * @param prestadorServico prestador afetado.
	 * @param horario          horário da afetação.
	 */
	public void guardarAfetacao(PedidoPrestacaoServico pedidoPrestacao, DescricaoServico servico, PrestadorServico prestadorServico, Date horario) {
		Afetacao afetacao = new Afetacao(pedidoPrestacao, servico, prestadorServico, horario);

		afetacoes.add(afetacao);
	}

	/**
	 * Verifica se uma descrição de serviço foi afetada.
	 *
	 * @param descricaoServico descriçao de serviço a verificar.
	 * @return se a descrição foi afetada.
	 */
	public boolean verificarSeServicoEstaAfetado(DescricaoServico descricaoServico) {
		for (Afetacao afetacao : afetacoes)
			if (afetacao.hasDescricao(descricaoServico))
				return true;

		return false;
	}

	/**
	 * Percorre todas as afetações, returnando um set daquelas cujo pedido é um requerido.
	 *
	 * @param pedido pedido requerido a verificar.
	 * @return um set com todas as afetações do pedido.
	 */
	public Set<Afetacao> getAfetacoesDePedido(PedidoPrestacaoServico pedido) {
		Set<Afetacao> res = new HashSet<>();

		for (Afetacao afetacao : afetacoes)
			if (afetacao.hasPedido(pedido)) res.add(afetacao);

		return res;
	}

	/**
	 * Elimina uma afetação da lista de afetações.
	 *
	 * @param afetacao afetação a eliminar.
	 */
	public void eliminarAfetacao(Afetacao afetacao) {
		afetacoes.remove(afetacao);
	}

}
