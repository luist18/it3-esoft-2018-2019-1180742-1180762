package pt.ipp.isep.dei.esoft.gpsd.ui.console;

import pt.ipp.isep.dei.esoft.gpsd.controller.ConsultarOrdensExecucaoServicoController;
import pt.ipp.isep.dei.esoft.gpsd.model.OrdemExecucaoServico;
import pt.ipp.isep.dei.esoft.gpsd.ui.console.utils.Utils;

public class ConsultarOrdensExecucaoServicoUI {

    private ConsultarOrdensExecucaoServicoController controller;

    public ConsultarOrdensExecucaoServicoUI() {
        controller = new ConsultarOrdensExecucaoServicoController();
    }

    public void run() {
        controller.novaConsulta();
        System.out.println("\nConsulta de ordens de execução de serviço:");
        if (introduzEstado()) {
            if (introduzFormato()) {
                apresentaDados();
                if (confirma()) {
                    controller.exportar();
                } else {
                    System.out.println("Consulta/exportação cancelada.");
                }
            } else {
                System.out.println("Formato inválido.");
            }
        } else {
            System.out.println("Estado inválido.");
        }

    }

    private boolean confirma() {
        String flag = Utils.readLineFromConsole("Deseja confirmar a exportação? (Y/N)");

        if (flag.equalsIgnoreCase("y")) {
            return true;
        } else {
            return false;
        }
    }

    private void apresentaDados() {
        for (OrdemExecucaoServico ordemExecucaoServico : controller.getLista()) {
            System.out.println(ordemExecucaoServico.toString());
        }

        System.out.println("Formato a exportar: " + controller.getExportador().getFormato());
    }

    private boolean introduzEstado() {
        String estado = Utils.readLineFromConsole("Estado (Por fazer, A fazer, Feito):");

        return controller.consultar(estado);
    }

    private boolean introduzFormato() {
        String formato = Utils.readLineFromConsole("Formato para exportar:");

        return controller.setFormato(formato);
    }

}
