package pt.ipp.isep.dei.esoft.gpsd.ui.console;

import pt.ipp.isep.dei.esoft.gpsd.controller.EspecificarAreaGeograficaController;
import pt.ipp.isep.dei.esoft.gpsd.model.CodigoPostal;
import pt.ipp.isep.dei.esoft.gpsd.ui.console.utils.Utils;

public class EspecificarAreaGeograficaUI {

    private EspecificarAreaGeograficaController controller;

    public EspecificarAreaGeograficaUI() {
        controller = new EspecificarAreaGeograficaController();
    }

    public void run() {
        System.out.println("\nEspecificar Serviço:");

        boolean confirmacao = false;
        while (!confirmacao) {
            if (introduzDados()) {
                if (controller.valida()) {
                    confirmacao = confirma();
                    if (confirmacao) {
                        if (controller.registaAreaGeografica()) {
                            System.out.println("Operação realizada com sucesso.");
                        } else {
                            System.out.println("Ocorreu um erro.");
                        }
                    } else {
                        System.out.println("Especificação de área geográfica cancelada.");
                        break;
                    }
                } else {
                    System.out.println("Ocorreu um erro.");
                }
            } else {
                System.out.println("Ocorreu um erro.");
            }
        }
    }

    private boolean confirma() {
        apresentaAreaGeo();
        String confirmacao = Utils.readLineFromConsole("Pretende registar o serviço? (Y/N)");
        if (confirmacao.equalsIgnoreCase("y")) {
            return true;
        } else {
            return false;
        }
    }

    private void apresentaAreaGeo() {
        System.out.println(controller.getAreaGeo().toString());
    }

    private boolean introduzDados() {
        String designacao = Utils.readLineFromConsole("Designação da área geográfica: ");
        double custo = Utils.readDoubleFromConsole("Custo da área geográfica: ");
        CodigoPostal codigoPostal = lerCodigoPostal();
        double raio = Utils.readDoubleFromConsole("Raio da área geográfica: ");

        if (controller.novaAreaGeografica(designacao, custo, codigoPostal, raio)) {
            return true;
        } else {
            return false;
        }
    }

    private CodigoPostal lerCodigoPostal() {
        String cod = Utils.readLineFromConsole("Codigo postal da área geográfica:");
        return new CodigoPostal(cod);
    }

}
