package pt.ipp.isep.dei.esoft.gpsd.controller;

import pt.ipp.isep.dei.esoft.gpsd.model.AreaGeografica;
import pt.ipp.isep.dei.esoft.gpsd.model.CandidaturaPrestadorServico;
import pt.ipp.isep.dei.esoft.gpsd.model.CategoriaServico;
import pt.ipp.isep.dei.esoft.gpsd.model.empresa.*;
import pt.ipp.isep.dei.esoft.gpsd.model.prestador.PrestadorServico;

import java.util.List;

/**
 * Classe responsável por gerir o caso de uso de registo de prestador de serviço.
 *
 * @author Márcio Duarte
 * @author Luís Tavares
 */
public class RegistarPrestadorServicoController {

	/**
	 * Empresa atual em questão.
	 */
	private Empresa empresa;

	/**
	 * Prestador de serviço que está a ser criado.
	 */
	private PrestadorServico prestadorServico;

	/**
	 * Instância que gere os prestadores de serviço.
	 */
	private RegistoPrestadoresServico registoPrestadoresServico;

	/**
	 * Instãncia que gere as áreas geográficas.
	 */
	private RegistoAreasGeograficas registoAreasGeograficas;

	/**
	 * Instância que gere as categorias de serviço.
	 */
	private RegistoCategoriasServico registoCategoriasServico;

	/**
	 * Caso haja uma candidatura para o prestador que se deseja registar, esta estará guardada aqui.
	 */
	private CandidaturaPrestadorServico candidaturaDoPrestador;

	/**
	 * Construtor que instância empresa e registos.
	 */
	public RegistarPrestadorServicoController() {
		AplicacaoGPSD m_oApp = AplicacaoGPSD.getInstance();
		this.empresa = m_oApp.getEmpresa();
		this.registoPrestadoresServico = empresa.getRegistoPrestadoresServico();
		this.registoAreasGeograficas = empresa.getRegistoAreasGeograficas();
		this.registoCategoriasServico = empresa.getRegistoCategoriasServico();
	}

	/**
	 * Verifica se existe uma candidatura associada ao prestador completando a instancia de candidatura.
	 *
	 * @param nif nif para verificar se candidatura existe.
	 * @return se existe candidatura com o prestador.
	 */
	public boolean verificarCandidaturas(String nif) {
		RegistoCandidaturasPrestadorServico registoCandidaturasPrestadorServico = empresa.getRegistoCandidaturasPrestadorServico();

		boolean existeCandidatura = registoCandidaturasPrestadorServico.existeCandidatura(nif);

		if (existeCandidatura)
			this.candidaturaDoPrestador = registoCandidaturasPrestadorServico.getCandidatura(nif);

		return existeCandidatura;
	}

	/**
	 * Cria um prestador de serviço.
	 *
	 * @param strNumero     numero mecanográfico de prestador a criar.
	 * @param nomeCompleto  nome completo do prestador.
	 * @param nomeAbreviado nome abreviado do prestador.
	 * @param email         email do prestador.
	 */
	public void criarPrestadorServico(String strNumero, String nomeCompleto, String nomeAbreviado, String email) {
		this.prestadorServico = registoPrestadoresServico.criarPrestadorServico(strNumero, nomeCompleto, nomeAbreviado, email);
	}

	/**
	 * Carrega as listas da candidatura para o prestador de serviço.
	 */
	public void carregarListasCandidatura() {
		this.prestadorServico.carregarListaAreasGeograficas(candidaturaDoPrestador.getAreasGeograficas());
		this.prestadorServico.carregarListaCategorias(candidaturaDoPrestador.getCategorias());
	}

	/**
	 * Verifica se um prestador de serviço é válido.
	 *
	 * @return se o prestador de serviço é válido.
	 */
	public boolean validarPrestadorServico() {
		return registoPrestadoresServico.validarPrestadorServico(prestadorServico);
	}

	/**
	 * Retorna todas as áreas geográficas existentes.
	 *
	 * @return todas as áreas geográficas existentes.
	 */
	public List<AreaGeografica> getAreasGeograficas() {
		return registoAreasGeograficas.getAreasGeograficas();
	}

	/**
	 * Adiciona uma área geográfica ao prestador tendo em conta o id escolhido.
	 *
	 * @param areaGeograficaId id da área geográfica.
	 * @return se a área geográfica foi devidamente adicionada.
	 */
	public boolean addAreaGeografica(String areaGeograficaId) {
		AreaGeografica areaGeografica = registoAreasGeograficas.getAreaGeograficaByID(areaGeograficaId);
		return prestadorServico.addAreaGeografica(areaGeografica);
	}

	/**
	 * Devolve as categorias de serviço em vigor.
	 *
	 * @return uma lista com as categorias de serviço.
	 */
	public List<CategoriaServico> getCategoriasServico() {
		return registoCategoriasServico.getCategoriasServico();
	}

	/**
	 * Adiciona uma categoria ao prestador pelo seu id.
	 *
	 * @param categoriaId id da categoria a adicionar.
	 * @return se a categoria foi devidamente adicionada.
	 */
	public boolean addCategoria(String categoriaId) {
		CategoriaServico categoriaServico = registoCategoriasServico.getCategoriaById(categoriaId);
		return prestadorServico.addCategoria(categoriaServico);
	}

	/**
	 * Regista o prestador de serviço.
	 */
	public void registarPrestadorServico() {
		String password = registoPrestadoresServico.gerarPalavraPasse();
		registoPrestadoresServico.registarPrestadorServico(prestadorServico, password);

		String email = prestadorServico.getEmail();
		//TODO: enviar email com serviço externo
	}

	/**
	 * Retorna o prestador de serviço a ser tratado.
	 *
	 * @return o prestador de serviço.
	 */
	public PrestadorServico getPrestadorServico() {
		return prestadorServico;
	}

	/**
	 * Retorna a candidatura associada ao prestador ou null caso não haja.
	 *
	 * @return uma candidatura ou null.
	 */
	public CandidaturaPrestadorServico getCandidatura() {
		return candidaturaDoPrestador;
	}
}
