package pt.ipp.isep.dei.esoft.gpsd.model;

import pt.ipp.isep.dei.esoft.gpsd.model.servico.Servico;

import java.util.Objects;

/**
 * Descrição de um serviço.
 *
 * @author Márcio Duarte
 * @author Luís Tavares
 */
public class DescricaoServico {

	/**
	 * Duração mínima de um serviço. Cada serviço deverá ter uma duração múltipla desta.
	 */
	private final int duracaoMinima = 30;

	/**
	 * Servico a qual a descrição se refere.
	 */
	private Servico servico;

	/**
	 * Descrição do serviço.
	 */
	private String descricao;

	/**
	 * Duração do serviço em minutos (deverá ser múltiplo da duração múltipla).
	 */
	private String duracao;

	/**
	 * Construtor com todos os atributos.
	 *
	 * @param servico   serviço ao qual a descrição se refere.
	 * @param descricao descição do serviço.
	 * @param duracao   duração do serviço em minutos (deverá ser múltiplo da duração múltipla).
	 */
	public DescricaoServico(Servico servico, String descricao, String duracao) {
		this.servico = servico;
		this.descricao = descricao;
		this.duracao = duracao;
	}

	/**
	 * Verifica se um pedido é válido.
	 *
	 * @return se o pedido é válido ou não.
	 */
	public boolean validarPedidoServico() {
		try {
			int duracao = Integer.parseInt(this.duracao);

			if (duracao % duracaoMinima != 0) return false;
		} catch (Exception e) {
			return false;
		}

		return true;
	}

	/**
	 * Retorna o serviço.
	 *
	 * @return o serviço.
	 */
	public Servico getServico() {
		return servico;
	}
}
