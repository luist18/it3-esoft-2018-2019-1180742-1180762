package pt.ipp.isep.dei.esoft.gpsd.controller;

import pt.ipp.isep.dei.esoft.autorizacao.model.SessaoUtilizador;
import pt.ipp.isep.dei.esoft.gpsd.model.Constantes;
import pt.ipp.isep.dei.esoft.gpsd.model.empresa.Empresa;
import pt.ipp.isep.dei.esoft.gpsd.model.empresa.RegistoPrestadoresServico;
import pt.ipp.isep.dei.esoft.gpsd.model.prestador.DisponibilidadeDiaria;
import pt.ipp.isep.dei.esoft.gpsd.model.prestador.ListaDisponibilidadeDiaria;
import pt.ipp.isep.dei.esoft.gpsd.model.prestador.PrestadorServico;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class IndicarDisponibilidadeDiariaController {

    private final Empresa empresa;

    private PrestadorServico prestadorServico;

    private ListaDisponibilidadeDiaria ldd;

    private DisponibilidadeDiaria disponibilidadeDiaria;
    private int reps;
    private DisponibilidadeDiaria.Padrao padrao;

    public IndicarDisponibilidadeDiariaController() {
        if (!AplicacaoGPSD.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_PRESTADOR_SERVICO))
            throw new IllegalStateException("Utilizador não Autorizado");
        this.empresa = AplicacaoGPSD.getInstance().getEmpresa();
    }

    public void indicarNovasDisponibilidades(){
        AplicacaoGPSD app = AplicacaoGPSD.getInstance();
        SessaoUtilizador sessao = app.getSessaoAtual();
        String email = sessao.getEmailUtilizador();
        RegistoPrestadoresServico registoPrestadores = empresa.getRegistoPrestadoresServico();
        prestadorServico = registoPrestadores.getPrestadorByEmail(email);
    }

    public void novoPeriodoDisponibilidade(LocalDate dataInicio, LocalDateTime horaInicio, LocalDate dataFim, LocalDateTime horaFim, DisponibilidadeDiaria.Padrao padrao, int reps){
        ldd = prestadorServico.getListaDisponibilidadeDiaria();
        disponibilidadeDiaria = ldd.novoPedidoDisponibilidade(dataInicio, horaInicio, dataFim, horaFim);
        this.padrao = padrao;
        this.reps = reps;
    }

    public boolean registaPedidoDisponibilidade(){
        return ldd.registaPeriodoDisponibilidade(disponibilidadeDiaria, padrao, reps);
    }

    public DisponibilidadeDiaria getDisponibilidade() {
        return this.disponibilidadeDiaria;
    }
}
