/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.controller;

import pt.ipp.isep.dei.esoft.gpsd.model.CategoriaServico;
import pt.ipp.isep.dei.esoft.gpsd.model.Constantes;
import pt.ipp.isep.dei.esoft.gpsd.model.empresa.Empresa;
import pt.ipp.isep.dei.esoft.gpsd.ui.console.utils.Utils;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author paulomaio
 */
public class EspecificarCategoriaController {
	private Empresa m_oEmpresa;
	private CategoriaServico m_oCategoriaServico;

	public EspecificarCategoriaController() {
		if (!AplicacaoGPSD.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_ADMINISTRATIVO))
			throw new IllegalStateException("Utilizador não Autorizado");
		this.m_oEmpresa = AplicacaoGPSD.getInstance().getEmpresa();
	}


	public boolean novaCategoria(String strCodigo, String strDescricao) {
		try {
			this.m_oCategoriaServico = this.m_oEmpresa.novaCategoria(strCodigo, strDescricao);
			return this.m_oEmpresa.validaCategoria(this.m_oCategoriaServico);
		} catch (RuntimeException ex) {
			Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
			this.m_oCategoriaServico = null;
			return false;
		}
	}


	public boolean registaCategoria() {
		return this.m_oEmpresa.registaCategoria(this.m_oCategoriaServico);
	}

	public String getCategoriaString() {
		return this.m_oCategoriaServico.toString();
	}
}
