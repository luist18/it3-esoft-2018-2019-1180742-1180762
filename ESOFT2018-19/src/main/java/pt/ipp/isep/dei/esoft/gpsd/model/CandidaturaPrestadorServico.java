package pt.ipp.isep.dei.esoft.gpsd.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Quando um utilizador se candidata a prestador de serviço cria uma candidatura.
 *
 * @author Márcio Duarte
 * @author Luís Tavares
 */
public class CandidaturaPrestadorServico {

	/**
	 * Nif do candidato.
	 */
	private String nif;

	/**
	 * Nome completo do candidato.
	 */
	private String nomeCompleto;

	/**
	 * Categorias do candidato.
	 */
	private List<CategoriaServico> categorias = new ArrayList<>();

	/**
	 * Áreas geográficas do candidato.
	 */
	private List<AreaGeografica> areasGeograficas = new ArrayList<>();

	public CandidaturaPrestadorServico(String nif, String nomeCompleto) {
		this.nif = nif;
		this.nomeCompleto = nomeCompleto;
	}

	/**
	 * Verifica se a candidatura tem um certo nif.
	 *
	 * @param nif nif a verificar.
	 * @return se a candidatura tem o nif ou não. I.e. um boolean.
	 */
	public boolean hasNif(String nif) {
		return this.nif.equals(nif);
	}

	public String getNif() {
		return nif;
	}

	public String getNomeCompleto() {
		return nomeCompleto;
	}

	public List<CategoriaServico> getCategorias() {
		return categorias;
	}

	public List<AreaGeografica> getAreasGeograficas() {
		return areasGeograficas;
	}
}
