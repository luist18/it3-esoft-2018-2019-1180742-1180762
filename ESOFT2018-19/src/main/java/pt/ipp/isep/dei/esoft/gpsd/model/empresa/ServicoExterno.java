package pt.ipp.isep.dei.esoft.gpsd.model.empresa;

import pt.ipp.isep.dei.esoft.gpsd.model.Atuacao;
import pt.ipp.isep.dei.esoft.gpsd.model.CodigoPostal;

import java.util.List;

public interface ServicoExterno {

    public List<Atuacao> getListaAtuacao(CodigoPostal codigoBase, double raio);

}
