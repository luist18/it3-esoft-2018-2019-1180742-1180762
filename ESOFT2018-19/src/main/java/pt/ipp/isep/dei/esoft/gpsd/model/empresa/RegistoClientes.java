package pt.ipp.isep.dei.esoft.gpsd.model.empresa;

import pt.ipp.isep.dei.esoft.autorizacao.AutorizacaoFacade;
import pt.ipp.isep.dei.esoft.gpsd.model.Constantes;
import pt.ipp.isep.dei.esoft.gpsd.model.EnderecoPostal;
import pt.ipp.isep.dei.esoft.gpsd.model.cliente.Cliente;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe responsável por controlar os clientes.
 *
 * @author Márcio Duarte
 * @author Luís Tavares
 */
public class RegistoClientes {

	/**
	 * Empresa na qual o registo está situado.
	 */
	private Empresa empresa;

	/**
	 * Lista com todos os clientes.
	 */
	private List<Cliente> listaClientes = new ArrayList<>();

	/**
	 * Construtor que inicializa a lista de clientes vazia.
	 *
	 * @param empresa empresa na qual o registo está situado.
	 */
	public RegistoClientes(Empresa empresa) {
		this.empresa = empresa;
	}

	/**
	 * Cria um novo Cliente.
	 *
	 * @param strNome     nome do cliente.
	 * @param strNif      nif do cliente em String.
	 * @param strTelefone telefone do cliente em String.
	 * @param strEmail    email do cliente.
	 * @param oMorada     EnderecoPostal do cliente.
	 * @return o cliente criado.
	 */
	public Cliente novoCliente(String strNome, String strNif, String strTelefone, String strEmail, EnderecoPostal oMorada) {
		return new Cliente(strNome, strNif, strTelefone, strEmail, oMorada);
	}

	/**
	 * Regista um cliente no registo.
	 *
	 * @param oCliente cliente a ser registado.
	 * @param strPwd   password do cliente.
	 * @return se o registo foi cumprido.
	 */
	public boolean registaCliente(Cliente oCliente, String strPwd) {
		AutorizacaoFacade oAutorizacao = empresa.getAutorizacaoFacade();

		if (this.validaCliente(oCliente, strPwd) && oCliente.valida()) {
			if (oAutorizacao.registaUtilizadorComPapel(oCliente.getNome(), oCliente.getEmail(), strPwd, Constantes.PAPEL_CLIENTE))
				return addCliente(oCliente);
		}
		return false;
	}

	/**
	 * Valida globalmente um cliente.
	 *
	 * @param oCliente cliente a ser validado.
	 * @param strPwd   sua password
	 * @return se o cliente é válido.
	 */
	public boolean validaCliente(Cliente oCliente, String strPwd) {
		boolean bRet = true;

		// Escrever aqui o código de validação
		if (existeCliente(oCliente.getEmail()))
			bRet = false;
		else if (strPwd == null)
			bRet = false;
		else if (strPwd.isEmpty())
			bRet = false;
		//

		return bRet;
	}

	/**
	 * Verifica se existe um cliente com um determinado email.
	 *
	 * @param clienteEmail email do cliente a verificar.
	 * @return se existe algum cliente com esse email.
	 */
	private boolean existeCliente(String clienteEmail) {
		Cliente cliente = getClienteByEmail(clienteEmail);
		return cliente != null;
	}

	/**
	 * Procura o cliente que tenha o referido email.
	 *
	 * @param email email a verificar.
	 * @return o cliente que tem o email, ou null caso nenhum o tenha.
	 */
	public Cliente getClienteByEmail(String email) {
		for (Cliente cliente : listaClientes) {
			if (cliente.getEmail().equals(email))
				return cliente;
		}
		return null;
	}

	/**
	 * Adiciona um cliente à lista de clientes.
	 *
	 * @param oCliente cliente a ser adicionado.
	 * @return se o cliente foi ccorretamente adicionado.
	 */
	private boolean addCliente(Cliente oCliente) {
		return listaClientes.add(oCliente);
	}
}
