package pt.ipp.isep.dei.esoft.gpsd.controller;

import pt.ipp.isep.dei.esoft.gpsd.model.CategoriaServico;
import pt.ipp.isep.dei.esoft.gpsd.model.Constantes;
import pt.ipp.isep.dei.esoft.gpsd.model.empresa.Empresa;
import pt.ipp.isep.dei.esoft.gpsd.model.empresa.RegistoCategoriasServico;
import pt.ipp.isep.dei.esoft.gpsd.model.empresa.RegistoServicos;
import pt.ipp.isep.dei.esoft.gpsd.model.servico.Servico;
import pt.ipp.isep.dei.esoft.gpsd.model.servico.TipoServico;

import java.util.List;


public class EspecificarServicoController {

    private Empresa empresa;

    private RegistoCategoriasServico registoCategorias;
    private RegistoServicos registoServicos;

    private Servico serv;

    public EspecificarServicoController() {
        if (!AplicacaoGPSD.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_ADMINISTRATIVO))
            throw new IllegalStateException("Utilizador não Autorizado");
        this.empresa = AplicacaoGPSD.getInstance().getEmpresa();
        this.registoCategorias = empresa.getRegistoCategorias();
        this.registoServicos = empresa.getRegistoServicos();
    }

    public List<CategoriaServico> getCategorias() {
        return registoCategorias.getCategoriasServico();
    }

    public boolean novoServico(String id, String descB, String descC, double custo, String catId, TipoServico ts) {
        CategoriaServico cat = registoCategorias.getCategoriaById(catId);

        if (catId != null) {
            this.serv = registoServicos.novoServico(id, descB, descC, custo, cat, ts);
            return true;
        } else {
            return false;
        }
    }

    public boolean valida() {
        return registoServicos.validaServico(serv);
    }

    public boolean registaServico() {
        return registoServicos.registaServico(serv);
    }

    public RegistoCategoriasServico getRegistoCategorias() {
        return registoCategorias;
    }

    public RegistoServicos getRegistoServicos() {
        return registoServicos;
    }

    public Servico getServico() {
        return serv;
    }
}
