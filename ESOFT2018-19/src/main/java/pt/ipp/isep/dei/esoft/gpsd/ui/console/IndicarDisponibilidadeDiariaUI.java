package pt.ipp.isep.dei.esoft.gpsd.ui.console;

import pt.ipp.isep.dei.esoft.gpsd.controller.IndicarDisponibilidadeDiariaController;
import pt.ipp.isep.dei.esoft.gpsd.model.prestador.DisponibilidadeDiaria;
import pt.ipp.isep.dei.esoft.gpsd.ui.console.utils.Utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class IndicarDisponibilidadeDiariaUI {

	private IndicarDisponibilidadeDiariaController controller;

	public IndicarDisponibilidadeDiariaUI() {
		controller = new IndicarDisponibilidadeDiariaController();
	}

	public void run() {
		controller.indicarNovasDisponibilidades();
		System.out.println("\nIndicar disponibilidade diária:");

		boolean confirmacao = false;
		while (!confirmacao) {
			introduzDados();
			if (controller.registaPedidoDisponibilidade()) {
				confirmacao = confirma();
				if (confirmacao) {
					if (controller.registaPedidoDisponibilidade()) {
						System.out.println("Disponibilidade registada com sucesso.");
					} else {
						System.out.println("Ocorreu um erro.");
					}
				} else {
					System.out.println("Indicação de disponibilidade diária cancelada.");
					break;
				}
			} else {
				System.out.println("Ocorreu um erro.");
			}
		}
	}

	public void introduzDados() {
		boolean valido;

		do {
			valido = true;
			try {
				DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("d/MM/yyyy HH:mm");
				String diaInicioString = Utils.readLineFromConsole("Dia inicio para registar (dd/MM/yyyy HH:mm):");
				String diaFimString = Utils.readLineFromConsole("Dia fim para registar (dd/MM/yyyy HH:mm):");
				String padraoString = Utils.readLineFromConsole("Padrao (Nenhum/Semanal/Mensal/Trimestral/Anual):");
				String repsString = Utils.readLineFromConsole("Número de repetições:");
				LocalDateTime horaInicio = LocalDateTime.parse(diaInicioString, dateTimeFormatter);
				LocalDateTime horaFim = LocalDateTime.parse(diaFimString, dateTimeFormatter);
				LocalDate date = LocalDate.from(horaInicio);

				DisponibilidadeDiaria.Padrao padrao;
				try {
					padrao = DisponibilidadeDiaria.Padrao.valueOf(padraoString.toUpperCase());
				} catch (Exception e) {
					padrao = null;
				}

				controller.novoPeriodoDisponibilidade(date, horaInicio, date, horaFim, padrao, Integer.parseInt(repsString));
			} catch (Exception e) {
				System.out.println("Introduziu dados incorretos, tente novamente");
				valido = false;
			}
		} while (!valido);
	}


	private boolean confirma() {
		apresentaDisponibilidade();
		String confirmacao = Utils.readLineFromConsole("Pretende registar a disponibilidade? (Y/N)");
		if (confirmacao.equalsIgnoreCase("y")) {
			return true;
		} else {
			return false;
		}
	}

	private void apresentaDisponibilidade() {
		System.out.println(controller.getDisponibilidade().toString());
	}
}
