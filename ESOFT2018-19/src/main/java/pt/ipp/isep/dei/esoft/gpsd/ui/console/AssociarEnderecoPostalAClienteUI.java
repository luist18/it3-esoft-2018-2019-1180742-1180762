package pt.ipp.isep.dei.esoft.gpsd.ui.console;

import pt.ipp.isep.dei.esoft.gpsd.controller.AssociarEnderecoPostalAClienteController;
import pt.ipp.isep.dei.esoft.gpsd.ui.console.utils.Utils;

/**
 * UI do Caso de Uso 7.
 *
 * @author Márcio Duarte
 * @author Luís Tavares
 */
public class AssociarEnderecoPostalAClienteUI {

	/**
	 * Controller associado a esta UI.
	 */
	private AssociarEnderecoPostalAClienteController controller;

	public AssociarEnderecoPostalAClienteUI() {
		controller = new AssociarEnderecoPostalAClienteController();
	}

	/**
	 * Método em que a UI se vai basear para correr.
	 */
	public void run() {
		boolean confirma;
		do {
			boolean enderecoCriado = criarEndereco();

			while (!enderecoCriado) {
				System.out.println("Endereço incorreto, por favor tente novamente.\n");
				enderecoCriado = criarEndereco();
			}

			confirma = apresentarDadosEPedirParaConfirmar();
		} while (!confirma);

		if (controller.guardarEndereco())
			System.out.println("Endereço registado!");
	}

	/**
	 * Pergunta as informações e cria um endereço dependendo das respostas.
	 *
	 * @return se o endereço foi devidamente criado.
	 */
	private boolean criarEndereco() {
		String endereco = Utils.readLineFromConsole("Endereço: ");
		String codPostalStr = Utils.readLineFromConsole("Código-postal: ");
		String localidade = Utils.readLineFromConsole("Localidade: ");

		return controller.novoEndereco(endereco, codPostalStr, localidade);
	}

	/**
	 * Apresenta o endereço postal e pergunta se quer que confirme.
	 *
	 * @return se o utilizador deseja confirmar ou não.
	 */
	private boolean apresentarDadosEPedirParaConfirmar() {
		System.out.println(controller.getDesignacaoEnderecoPostal());
		return Utils.confirma("Confirma este endereço postal? ");
	}
}
