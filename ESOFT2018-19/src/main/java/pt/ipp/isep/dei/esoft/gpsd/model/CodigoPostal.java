package pt.ipp.isep.dei.esoft.gpsd.model;

/**
 * Código postal é uma referência de localização.
 *
 * @author Márcio Duarte
 * @author Luís Tavares
 */
public class CodigoPostal {

	/**
	 * Endereço de código postal.
	 */
	private String codigoPostal;

	/**
	 * Construtor de código postal.
	 *
	 * @param codigoPostal código postal associado em String.
	 */
	public CodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	/**
	 * toString que transforma o codigo postal numa string com o código postal.
	 *
	 * @return uma string com o código postal.
	 */
	@Override
	public String toString() {
		return codigoPostal;
	}
}
