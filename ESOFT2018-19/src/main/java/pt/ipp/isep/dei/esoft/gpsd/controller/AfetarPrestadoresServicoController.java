package pt.ipp.isep.dei.esoft.gpsd.controller;

import pt.ipp.isep.dei.esoft.gpsd.model.AlgoritmoEscalonamento;
import pt.ipp.isep.dei.esoft.gpsd.model.PedidoPrestacaoServico;
import pt.ipp.isep.dei.esoft.gpsd.model.empresa.Empresa;
import pt.ipp.isep.dei.esoft.gpsd.model.empresa.RegistoAfetacoes;
import pt.ipp.isep.dei.esoft.gpsd.model.empresa.RegistoPedidosPrestacaoServico;
import pt.ipp.isep.dei.esoft.gpsd.model.empresa.RegistoPrestadoresServico;
import pt.ipp.isep.dei.esoft.gpsd.model.prestador.PrestadorServico;

import java.util.List;

/**
 * Classe responsável por controlar o caso de uso para afetar prestadores de serviço.
 *
 * @author Márcio Duarte
 * @author Luís Tavares
 */
public class AfetarPrestadoresServicoController {

	/**
	 * Empresa em vigor.
	 */
	private Empresa empresa;

	/**
	 * Construtor que inicia as instâncias necessárias.
	 */
	public AfetarPrestadoresServicoController() {
		this.empresa = AplicacaoGPSD.getInstance().getEmpresa();
	}

	/**
	 * Corre a afetação de pedidos de prestaçao de seviço.
	 */
	public void afetarPrestadoresServico() {
		RegistoAfetacoes registoAfetacoes = empresa.getRegistoAfetacoes();

		List<PedidoPrestacaoServico> pedidosSubmetidos = getListaPedidosSubmetidos();
		List<PrestadorServico> prestadoresComDisponbilidade = getListaPrestadoresDisponiveis();
		afetarPrestadoresServico(pedidosSubmetidos, prestadoresComDisponbilidade, registoAfetacoes);

		for (PedidoPrestacaoServico pedido : pedidosSubmetidos)
			if (pedido.verificarSeServicosForamAfetados(registoAfetacoes)) {
				DecidirSobrePeriodoPropostoController controller = new DecidirSobrePeriodoPropostoController();
				controller.notificarUtilizador(pedido);
			}
	}

	/**
	 * Afeta os prestadores de serviço aos serviços usando o algoritmo de escalonamento.
	 *
	 * @param pedidosSubmetidos            lista de pedidos que estejam submetidos.
	 * @param prestadoresComDisponbilidade lista de prestadores disponíveis.
	 */
	private void afetarPrestadoresServico(List<PedidoPrestacaoServico> pedidosSubmetidos,
										  List<PrestadorServico> prestadoresComDisponbilidade,
										  RegistoAfetacoes registoAfetacoes) {
		AlgoritmoEscalonamento algoritmoEscalonamento = empresa.getAlgoritmoEscalonamento();

		algoritmoEscalonamento.afetarPrestadoresServico(pedidosSubmetidos, prestadoresComDisponbilidade, registoAfetacoes);
	}

	/**
	 * Encontra a lista com todos os prestadores de serviço disponíveis.
	 *
	 * @return lista com todos os prestadores de serviço disponíveis.
	 */
	private List<PrestadorServico> getListaPrestadoresDisponiveis() {
		RegistoPrestadoresServico registoPrestadoresServico = empresa.getRegistoPrestadoresServico();
		return registoPrestadoresServico.getPrestadoresComDisponibilidade();
	}

	/**
	 * Encontra uma lista com todos os pedidos submetidos.
	 *
	 * @return uma lista com todos os pedidos de prestação submetidos.
	 */
	private List<PedidoPrestacaoServico> getListaPedidosSubmetidos() {
		RegistoPedidosPrestacaoServico registoPedidosPrestacaoServico = empresa.getRegistoPedidosPrestacaoServico();
		return registoPedidosPrestacaoServico.getPedidosSubmetidos();
	}
}
