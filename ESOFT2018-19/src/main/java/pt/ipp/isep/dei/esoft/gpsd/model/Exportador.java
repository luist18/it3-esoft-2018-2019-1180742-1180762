package pt.ipp.isep.dei.esoft.gpsd.model;

import java.util.List;

public interface Exportador {

    void exportar(List<OrdemExecucaoServico> listaOrdens);

    String getFormato();

}
