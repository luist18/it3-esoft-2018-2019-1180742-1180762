package pt.ipp.isep.dei.esoft.gpsd.ui.console;

import pt.ipp.isep.dei.esoft.gpsd.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class MenuPrestadorUI {

    public MenuPrestadorUI() {
    }

    public void run() {
        List<String> options = new ArrayList<String>();
        options.add("Indicar disponibilidade diária");
        options.add("Consultar ordens execução de serviço");

        int opcao = 0;
        do {
            opcao = Utils.apresentaESelecionaIndex(options, "\n\nMenu Prestador");

            switch (opcao) {
                case 0:
                    IndicarDisponibilidadeDiariaUI indicarDisponibilidadeDiariaUI = new IndicarDisponibilidadeDiariaUI();
                    indicarDisponibilidadeDiariaUI.run();
                    break;
                case 1:
                    ConsultarOrdensExecucaoServicoUI consultarOrdensExecucaoServicoUI = new ConsultarOrdensExecucaoServicoUI();
                    consultarOrdensExecucaoServicoUI.run();
                    break;
            }

            // Incluir as restantes opções aqui

        }
        while (opcao != -1);
    }
}
