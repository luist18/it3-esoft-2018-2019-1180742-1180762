package pt.ipp.isep.dei.esoft.gpsd.controller;

import pt.ipp.isep.dei.esoft.autorizacao.model.SessaoUtilizador;
import pt.ipp.isep.dei.esoft.gpsd.model.CategoriaServico;
import pt.ipp.isep.dei.esoft.gpsd.model.EnderecoPostal;
import pt.ipp.isep.dei.esoft.gpsd.model.PedidoPrestacaoServico;
import pt.ipp.isep.dei.esoft.gpsd.model.cliente.Cliente;
import pt.ipp.isep.dei.esoft.gpsd.model.empresa.*;
import pt.ipp.isep.dei.esoft.gpsd.model.servico.Servico;

import java.util.List;

/**
 * Controller que controla o UC6 - efetuação de pedido de prestação de serviços.
 */
public class EfetuarPedidoPrestacaoServicoController {

	/**
	 * Pedido de prestação de serviço a ser inicializado.
	 */
	private PedidoPrestacaoServico pedidoPrestacaoServico;

	/**
	 * Instância que gere os pedidos de prestação de serviço.
	 */
	private RegistoPedidosPrestacaoServico registoPedidosPrestacaoServico;

	/**
	 * Instãncia que gere todos os clientes.
	 */
	private RegistoClientes registoClientes;

	/**
	 * Instãncia que gere as categorias de serviço.
	 */
	private RegistoCategoriasServico registoCategoriasServico;

	/**
	 * Instância que gere os serviços da empresa.
	 */
	private RegistoServicos registoServicos;

	/**
	 * Cliente que está a executar o UC.
	 */
	private Cliente cliente;

	/**
	 * Instância da aplicação.
	 */
	private AplicacaoGPSD app;

	/**
	 * Construtor.
	 */
	public EfetuarPedidoPrestacaoServicoController() {
		this.app = AplicacaoGPSD.getInstance();
		Empresa empresa = app.getEmpresa();

		this.registoClientes = empresa.getRegistoClientes();
		this.registoPedidosPrestacaoServico = empresa.getRegistoPedidosPrestacaoServico();
		this.registoCategoriasServico = empresa.getRegistoCategoriasServico();
		this.registoServicos = empresa.getRegistoServicos();
	}

	/**
	 * Cria um pedido de prestação de serviços.
	 */
	public void inicializarDados() {
		SessaoUtilizador sessao = app.getSessaoAtual();
		String email = sessao.getEmailUtilizador();
		this.cliente = registoClientes.getClienteByEmail(email);
	}

	/**
	 * Devolve uma lista com todos os endereços postais do cliente.
	 *
	 * @return uma lista com todos os endereços postais do cliente.
	 */
	public List<EnderecoPostal> getEnderecosPostais() {
		return (cliente == null) ? null : cliente.getEnderecosPostais();
	}

	/**
	 * Cria um pedido de prestação de serviços.
	 *
	 * @param enderecoPostal endereço postal onde a prestação se irá realizar.s
	 */
	public void criarPedido(EnderecoPostal enderecoPostal) {
		pedidoPrestacaoServico = registoPedidosPrestacaoServico.novoPedido(cliente, enderecoPostal);
	}

	/**
	 * Retorna uma lista com todas as categorias de serviço.
	 *
	 * @return uma lista com todas as categorias de serviço.
	 */
	public List<CategoriaServico> getCategorias() {
		return registoCategoriasServico.getCategoriasServico();
	}

	/**
	 * Retorna uma lista com os serviços de uma categoria especifica.
	 *
	 * @param categoriaID id da categoria que se quer.
	 * @return uma lista com os serviços de determinada categoria.
	 */
	public List<Servico> getServicosDeCategoria(String categoriaID) {
		return registoServicos.getServicosDeCategoria(categoriaID);
	}

	/**
	 * Adicona um pedido de serviço ao pedido de prestacao de serviços.
	 *
	 * @param servico    serviço a adicionar.
	 * @param descricao  descrição do serviço.
	 * @param strDuracao duração do serviço.
	 * @return se o pedido foi corretamente adicionado.
	 */
	public boolean addPedidoServico(Servico servico, String descricao, String strDuracao) {
		return pedidoPrestacaoServico.addPedidoServico(servico, descricao, strDuracao);
	}

	/**
	 * Adiciona horário preferencial ao pedido de prestação de serviços.
	 *
	 * @param strData data do horário.
	 * @param strHora hora do horário.
	 * @return se a adição foi concluida.
	 */
	public boolean addHorario(String strData, String strHora) {
		return pedidoPrestacaoServico.addHorario(strData, strHora);
	}

	/**
	 * Terminar o pedido e valida-o.
	 *
	 * @return se o pedido é válido.
	 */
	public boolean valida() {
		return registoPedidosPrestacaoServico.validarPedido(pedidoPrestacaoServico);
	}

	/**
	 * Devolve o pedido de prestacao de serviços em vigor.
	 *
	 * @return o pedido de prestacao de serviços em vigor.
	 */
	public PedidoPrestacaoServico getPedidoPrestacaoServico() {
		return pedidoPrestacaoServico;
	}

	/**
	 * Regista o pedido de prestação.
	 *
	 * @return o número com o qual o registo ficou identificado.
	 */
	public int registaPedido() {
		return registoPedidosPrestacaoServico.registarPedido(pedidoPrestacaoServico);
		//TODO: serviço externo para enviar email com informações
	}
}
