package pt.ipp.isep.dei.esoft.gpsd.model;

import pt.ipp.isep.dei.esoft.gpsd.model.prestador.PrestadorServico;

import java.util.Date;

/**
 * O que interliga os serviços aos horários e prestadores.
 *
 * @author Márcio Duarte
 * @author Luís Tavares
 */
public class Afetacao {

	/**
	 * Estado da afetação.
	 */
	private EstadoAfetacao estado;

	/**
	 * Pedido de prestação de seviço à qual a afetação se refere.
	 */
	private PedidoPrestacaoServico pedidoPrestacaoServico;

	/**
	 * Descrição de serviço referente à afetação.
	 */
	private DescricaoServico descricaoServico;

	/**
	 * Horario correspondente à afetação.
	 */
	private Date horario;

	/**
	 * Prestador de serviço associado à afetação.
	 */
	private PrestadorServico prestadorServico;

	/**
	 * Construtor que coloca a afetação como submetida.
	 *
	 * @param pedidoPrestacaoServico pedido de prestação associado.
	 * @param descricaoServico       descrição do serviço associado.
	 * @param prestadorServico       prestador que vai ser afetado.
	 * @param horario                horario correspondente à afetação.
	 */
	public Afetacao(PedidoPrestacaoServico pedidoPrestacaoServico, DescricaoServico descricaoServico,
					PrestadorServico prestadorServico, Date horario) {
		this.pedidoPrestacaoServico = pedidoPrestacaoServico;
		this.descricaoServico = descricaoServico;
		this.prestadorServico = prestadorServico;
		this.horario = horario;

		this.estado = EstadoAfetacao.SUBMETIDA;
	}

	/**
	 * Verifica se a afetação tem a descrição de serviço requerida.
	 *
	 * @param descricaoServico descrição de serviço requerida.
	 * @return se a afetação tem a desrição requerida.
	 */
	public boolean hasDescricao(DescricaoServico descricaoServico) {
		return this.descricaoServico.equals(descricaoServico);
	}

	/**
	 * Verifica se o pedido na afetação é o mesmo que um requerido.
	 *
	 * @param pedido pedido requerido a verificar.
	 * @return se o pedido é o mesmo.
	 */
	public boolean hasPedido(PedidoPrestacaoServico pedido) {
		return this.pedidoPrestacaoServico.equals(pedido);
	}

	/**
	 * Confirma uma afetação mudando-lhe o estado.
	 */
	public void confirmar() {
		this.estado = EstadoAfetacao.CONFIRMADA;
	}

	/**
	 * Retorna o pedido de prestação de serviços associado.
	 *
	 * @return o pedido de prestação de serviços associado.
	 */
	public PedidoPrestacaoServico getPedidoPrestacaoServico() {
		return pedidoPrestacaoServico;
	}

	/**
	 * Retorna a descrição de serviço associada.
	 *
	 * @return a descrição de serviço associada.
	 */
	public DescricaoServico getDescricaoServico() {
		return descricaoServico;
	}

	/**
	 * Retorna o horário da afetação.
	 *
	 * @return o horário da afetação.
	 */
	public Date getHorario() {
		return horario;
	}

	/**
	 * Retorna o prestador de serviço associado.
	 *
	 * @return o prestador de serviço associado.
	 */
	public PrestadorServico getPrestadorServico() {
		return prestadorServico;
	}

	/**
	 * Enum que descreve o estado de uma afetação.
	 */
	private enum EstadoAfetacao {
		SUBMETIDA, CONFIRMADA
	}
}
