package pt.ipp.isep.dei.esoft.gpsd.controller;

import pt.ipp.isep.dei.esoft.autorizacao.model.SessaoUtilizador;
import pt.ipp.isep.dei.esoft.gpsd.model.Constantes;
import pt.ipp.isep.dei.esoft.gpsd.model.Exportador;
import pt.ipp.isep.dei.esoft.gpsd.model.OrdemExecucaoServico;
import pt.ipp.isep.dei.esoft.gpsd.model.empresa.Empresa;
import pt.ipp.isep.dei.esoft.gpsd.model.empresa.RegistoExportadores;
import pt.ipp.isep.dei.esoft.gpsd.model.empresa.RegistoOrdensExecucaoServico;
import pt.ipp.isep.dei.esoft.gpsd.model.empresa.RegistoPrestadoresServico;
import pt.ipp.isep.dei.esoft.gpsd.model.prestador.PrestadorServico;

import java.util.List;

public class ConsultarOrdensExecucaoServicoController {

    private final Empresa empresa;

    private PrestadorServico prestadorServico;

    private RegistoOrdensExecucaoServico ro;

    private RegistoExportadores re;

    private List<OrdemExecucaoServico> lista;

    private Exportador exportador;

    public ConsultarOrdensExecucaoServicoController(){
        if (!AplicacaoGPSD.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_PRESTADOR_SERVICO))
            throw new IllegalStateException("Utilizador não Autorizado");
        this.empresa = AplicacaoGPSD.getInstance().getEmpresa();
    }

    public void novaConsulta(){
        AplicacaoGPSD app = AplicacaoGPSD.getInstance();
        SessaoUtilizador sessao = app.getSessaoAtual();
        String email = sessao.getEmailUtilizador();
        RegistoPrestadoresServico registoPrestadores = empresa.getRegistoPrestadoresServico();
        this.prestadorServico = registoPrestadores.getPrestadorByEmail(email);
        this.ro = empresa.getRegistoOrdensExecucaoServico();
        this.re = empresa.getRegistoExportadores();
    }

    public boolean consultar(String estado){
        List<OrdemExecucaoServico> lista = ro.consultar(estado, prestadorServico);
        if (lista == null){
            return false;
        } else {
            this.lista = lista;
            return true;
        }
    }

    public boolean setFormato(String formato){
        Exportador exportador = re.getExpotadorByFormato(formato);
        if (exportador == null){
            return false;
        } else {
            this.exportador = exportador;
            return true;
        }
    }

    public void exportar(){
        exportador.exportar(lista);
    }

    public List<OrdemExecucaoServico> getLista(){
        return lista;
    }

    public Exportador getExportador(){
        return exportador;
    }
}
