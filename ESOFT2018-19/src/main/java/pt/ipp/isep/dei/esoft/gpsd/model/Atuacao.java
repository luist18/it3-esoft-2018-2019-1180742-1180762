package pt.ipp.isep.dei.esoft.gpsd.model;

public class Atuacao {

    private EnderecoPostal enderecoPostal;

    private double distancia;

    public Atuacao(EnderecoPostal enderecoPostal, double distancia) {
        this.enderecoPostal = enderecoPostal;
        this.distancia = distancia;
    }

    public double getDistancia() {
        return distancia;
    }

    public EnderecoPostal getEnderecoPostal() {
        return enderecoPostal;
    }
}
