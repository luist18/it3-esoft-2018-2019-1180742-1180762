package pt.ipp.isep.dei.esoft.gpsd.model.empresa;

import pt.ipp.isep.dei.esoft.gpsd.model.AreaGeografica;
import pt.ipp.isep.dei.esoft.gpsd.model.Atuacao;
import pt.ipp.isep.dei.esoft.gpsd.model.CodigoPostal;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe responsável pela gestão das áreas geográficas.
 *
 * @author Márcio Duarte
 * @author Luís Tavares
 */
public class RegistoAreasGeograficas {

    /**
     * Lista com todas as áreas geográficas.
     */
	private List<AreaGeografica> areasGeograficas = new ArrayList<>();

    /**
     * Construtor.
     */
    public RegistoAreasGeograficas() {
        //TODO: complete
    }

    /**
     * Cria uma área geográfica.
     *
     * @param designacao       designação da área geográfica.
     * @param custo            custo associado à área geográfica.
     * @param codigoPostalBase código postal base da área.
     * @param raio             raio da área geográfica.
     * @param listaAtuacao     listaAtuação da área geográfica.
     * @return área geográfica criada.
     */
    public AreaGeografica novaAreaGeografica(String designacao, double custo, CodigoPostal codigoPostalBase, double raio, List<Atuacao> listaAtuacao) {
        return new AreaGeografica(designacao, custo, codigoPostalBase, raio, listaAtuacao);
    }

    /**
     * Regista uma área geográfica.
     *
     * @param areaGeografica área geográfica a registar.
     * @return se a área geográfica foi devidamente registada.
     */
    public boolean registaAreaGeografica(AreaGeografica areaGeografica) {
        return this.areasGeograficas.add(areaGeografica);
    }

    /**
     * Encontra uma área geográfica com base no ID.
     *
     * @param ID ID da área geográfica.
     * @return a área geográfica respetiva ao ID.
     */
    public AreaGeografica getAreaGeograficaByID(String ID) {
        for (AreaGeografica area : this.areasGeograficas) {
            //if (area.hasID(ID))
            //return area;
        }
        return null;
    }

    /**
     * Valida uma área geográfica.
     *
     * @param areaGeografica área geográfica a ser avaliada.
     * @return se a área gegoráfica é válida ou não.
     */
    public boolean validar(AreaGeografica areaGeografica) {
        return verifica(areaGeografica) && areaGeografica.validar();
    }

    /**
     * Retorna uma lista com todas as áreas geográficas.
     *
     * @return uma lista com todas as áreas geográficas.
     */
    public List<AreaGeografica> getAreasGeograficas() {
        return new ArrayList<>(areasGeograficas);
    }

    private boolean verifica(AreaGeografica areaGeografica) {
        return !areasGeograficas.contains(areaGeografica);
    }
}
