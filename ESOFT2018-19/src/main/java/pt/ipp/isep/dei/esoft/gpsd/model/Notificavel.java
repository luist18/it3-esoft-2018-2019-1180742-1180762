package pt.ipp.isep.dei.esoft.gpsd.model;

/**
 * Interface para controllers que sejam notificaveis. I.e. possíveis de correr quando um utilizador acede ao sistema.
 *
 * @author Márcio Duarte
 * @author Luís Tavares
 */
public interface Notificavel {

	/**
	 * Corre a notificação quando o utilizador acede ao sistema.
	 */
	void runNotificao();
}
