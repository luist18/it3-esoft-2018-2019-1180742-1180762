package pt.ipp.isep.dei.esoft.gpsd.model;

public class Intervalo {

    private String horaInicio, horaFim;

    public Intervalo(String horaInicio, String horaFim) {
        this.horaInicio = horaInicio;
        this.horaFim = horaFim;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public String getHoraFim() {
        return horaFim;
    }

    public boolean validar() {
        return true;
    }
}
