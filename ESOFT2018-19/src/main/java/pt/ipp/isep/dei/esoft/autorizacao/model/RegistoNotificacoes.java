package pt.ipp.isep.dei.esoft.autorizacao.model;

import pt.ipp.isep.dei.esoft.gpsd.model.Notificavel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Registo onde são guardadas todas as notificações.
 */
public class RegistoNotificacoes {

	/**
	 * Onde estão guardadas todas as notificações.
	 */
	private Set<Notificacao> notificacoes = new HashSet<>();

	/**
	 * Construtor vazio.
	 */
	public RegistoNotificacoes() {
	}

	/**
	 * Cria uma nova notificaçsão.
	 *
	 * @param emailDoUtilizador email do utilizador para notificado.
	 * @param controller        controller para onde o utilizador será redirecionado.
	 */
	public void criarNotificacao(String emailDoUtilizador, Notificavel controller) {
		Notificacao notificacao = new Notificacao(emailDoUtilizador, controller);

		this.notificacoes.add(notificacao);
	}

	/**
	 * Encontra todas as notificações de um utilizador pelo seu email.
	 *
	 * @param emailDoUtilizador email do utilizador.
	 * @return todas as notificações do utilizador.
	 */
	public List<Notificacao> getNotificacao(String emailDoUtilizador) {
		List<Notificacao> res = new ArrayList<>();

		for (Notificacao notificacao : notificacoes)
			if (notificacao.isDoUtilizador(emailDoUtilizador))
				res.add(notificacao);

		return res;
	}
}
