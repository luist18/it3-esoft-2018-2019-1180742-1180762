package pt.ipp.isep.dei.esoft.gpsd.model.servico;

import pt.ipp.isep.dei.esoft.gpsd.model.CategoriaServico;

public class ServicoFixo implements Servico {

    private String id;
    private String descricaoBreve;
    private String descricaoCompleta;
    private double custo;
    private double duracaoPreDeterminada;
    private CategoriaServico categoriaServico;

    public ServicoFixo(String id, String descricaoBreve, String descricaoCompleta, double custo, CategoriaServico categoriaServico) {
        if ((id == null) || (descricaoBreve == null) || (descricaoCompleta == null) ||
                (custo < 0) || (categoriaServico == null) ||
                (id.isEmpty()) || (descricaoBreve.isEmpty()) || (descricaoCompleta.isEmpty()))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        this.id = id;
        this.descricaoBreve = descricaoBreve;
        this.descricaoCompleta = descricaoCompleta;
        this.custo = custo;
        this.categoriaServico = categoriaServico;
    }

    @Override
    public boolean possuiOutrosAtributos() {
        return true;
    }

    @Override
    public double getOutrosAtributos() {
        return duracaoPreDeterminada;
    }

    @Override
    public void setOutrosAtributos(double atrib) {
        if (possuiOutrosAtributos()) this.duracaoPreDeterminada = atrib;
    }

    @Override
    public boolean validaServico() {
        return true;
    }

    @Override
    public double getCusto() {
        return custo;
    }

    @Override
    public CategoriaServico getCategoria() {
        return categoriaServico;
    }
}
