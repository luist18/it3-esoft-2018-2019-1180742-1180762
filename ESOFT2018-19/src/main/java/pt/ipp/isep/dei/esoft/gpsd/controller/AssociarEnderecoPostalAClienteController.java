package pt.ipp.isep.dei.esoft.gpsd.controller;

import pt.ipp.isep.dei.esoft.autorizacao.model.SessaoUtilizador;
import pt.ipp.isep.dei.esoft.gpsd.model.EnderecoPostal;
import pt.ipp.isep.dei.esoft.gpsd.model.cliente.Cliente;
import pt.ipp.isep.dei.esoft.gpsd.model.cliente.ListaEnderecosPostais;
import pt.ipp.isep.dei.esoft.gpsd.model.empresa.Empresa;
import pt.ipp.isep.dei.esoft.gpsd.model.empresa.RegistoClientes;

/**
 * Controller do Caso de Uso 7.
 *
 * @author Márcio Duarte
 * @author Luís Tavares
 */
public class AssociarEnderecoPostalAClienteController {

	/**
	 * Endereço postal que está a ser criado.
	 */
	private EnderecoPostal enderecoPostal;

	/**
	 * Lista que contém todos os endereços postais do utilizador.
	 */
	private ListaEnderecosPostais listaEnderecosPostais;

	/**
	 * Cria um novo endereço postal.
	 *
	 * @param endereco       endereço ou local do endereço postal.
	 * @param codioPostalStr codigo postal do endereço postal.
	 * @param localidade     localidade do endereço postal.
	 * @return se o endereço foi devidamente criado ou não.
	 */
	public boolean novoEndereco(String endereco, String codioPostalStr, String localidade) {
		AplicacaoGPSD app = AplicacaoGPSD.getInstance();
		SessaoUtilizador sessao = app.getSessaoAtual();
		Empresa empresa = app.getEmpresa();
		String email = sessao.getEmailUtilizador();
		RegistoClientes registoClientes = empresa.getRegistoClientes();
		Cliente cliente = registoClientes.getClienteByEmail(email);
		listaEnderecosPostais = cliente.getListaEnderecosPostais();

		enderecoPostal = listaEnderecosPostais.criarEndereco(endereco, codioPostalStr, localidade);

		return listaEnderecosPostais.validarEndereco(enderecoPostal);
	}

	/**
	 * Guarda um endereço postal na lista de um Utilizador.
	 *
	 * @return se o endereço foi devidamente guardado.
	 */
	public boolean guardarEndereco() {
		return listaEnderecosPostais.addEndereco(enderecoPostal);
	}

	/**
	 * Devolve uma designação para o endereço postal a ser tratado.
	 *
	 * @return designação para o endereço postal a ser tratado.
	 */
	public String getDesignacaoEnderecoPostal() {
		return enderecoPostal.toString();
	}
}
