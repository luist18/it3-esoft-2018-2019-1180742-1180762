package pt.ipp.isep.dei.esoft.gpsd.model.servico;

import pt.ipp.isep.dei.esoft.gpsd.model.CategoriaServico;

public class ServicoExpansivel implements Servico {

    private String id;
    private String descricaoBreve;
    private String descricaoCompleta;
    private double custo;
    private CategoriaServico categoriaServico;

    public ServicoExpansivel(String id, String descricaoBreve, String descricaoCompleta, double custo, CategoriaServico categoriaServico) {
        if ((id == null) || (descricaoBreve == null) || (descricaoCompleta == null) ||
                (custo < 0) || (categoriaServico == null) ||
                (id.isEmpty()) || (descricaoBreve.isEmpty()) || (descricaoCompleta.isEmpty()))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        this.id = id;
        this.descricaoBreve = descricaoBreve;
        this.descricaoCompleta = descricaoCompleta;
        this.custo = custo;
        this.categoriaServico = categoriaServico;
    }

    @Override
    public boolean possuiOutrosAtributos() {
        return false;
    }

    @Override
    public double getOutrosAtributos() {
        return 0;
    }

    @Override
    public void setOutrosAtributos(double atrib) {
    }

    @Override
    public boolean validaServico() {
        return true;
    }

    @Override
    public double getCusto() {
        return custo;
    }

    @Override
    public CategoriaServico getCategoria() {
        return categoriaServico;
    }


}
