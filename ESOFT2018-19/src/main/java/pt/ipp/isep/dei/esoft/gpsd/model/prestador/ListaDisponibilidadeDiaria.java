package pt.ipp.isep.dei.esoft.gpsd.model.prestador;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class ListaDisponibilidadeDiaria {

    private List<DisponibilidadeDiaria> listaDisponibilidadeDiaria;

    public ListaDisponibilidadeDiaria() {
        listaDisponibilidadeDiaria = new ArrayList<>();
    }

    public DisponibilidadeDiaria novoPedidoDisponibilidade(LocalDate dataInicio, LocalDateTime horaInicio, LocalDate dataFim, LocalDateTime horaFim) {
        return new DisponibilidadeDiaria(dataInicio, horaInicio, dataFim, horaFim);
    }

    public boolean registaPeriodoDisponibilidade(DisponibilidadeDiaria disp, DisponibilidadeDiaria.Padrao padrao, int reps) {
        if (!validaPeriodoDisponibilidade(disp)) return false;
        return registaComPadrao(disp, padrao, reps);
    }

	public boolean isDisponivel() {
		return !listaDisponibilidadeDiaria.isEmpty();
	}

    private boolean registaComPadrao(DisponibilidadeDiaria disp, DisponibilidadeDiaria.Padrao padrao, int reps) {
        boolean flag = false;
        for (int i = 0; i <= reps; i++) {
            LocalDate data = disp.getDataInicio();
            switch (padrao) {
                case SEMANAL:
                    data.plusWeeks(1 * i);
                    break;
                case MENSAL:
                    data.plusMonths(1 * i);
                    break;
                case TRIMESTRAL:
                    data.plusMonths(3 * i);
                    break;
                case ANUAL:
                    data.plusYears(1 * i);
                    break;
                default:
                    break;
            }

            DisponibilidadeDiaria novaDisp = new DisponibilidadeDiaria(data, disp.getHoraInicio(),
                    data, disp.getHoraFim());
            flag = listaDisponibilidadeDiaria.add(novaDisp);
        }
        return flag;
    }

    private boolean validaPeriodoDisponibilidade(DisponibilidadeDiaria disp) {
        if (disp.getHoraFim().compareTo(disp.getHoraInicio()) > 0 && disp.getDataInicio().equals(disp.getDataFim())) {
            return true;
        }

        return false;
    }

}
