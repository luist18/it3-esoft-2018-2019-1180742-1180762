package pt.ipp.isep.dei.esoft.gpsd.model.prestador;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class DisponibilidadeDiaria {

    private LocalDate dataInicio, dataFim;

    private LocalDateTime horaInicio, horaFim;

    public DisponibilidadeDiaria(LocalDate dataInicio, LocalDateTime horaInicio, LocalDate dataFim, LocalDateTime horaFim) {
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
        this.horaInicio = horaInicio;
        this.horaFim = horaFim;
    }

    public LocalDate getDataInicio() {
        return dataInicio;
    }

    public LocalDate getDataFim() {
        return dataFim;
    }

    public LocalDateTime getHoraInicio() {
        return horaInicio;
    }

    public LocalDateTime getHoraFim() {
        return horaFim;
    }

    public enum Padrao {
        SEMANAL, MENSAL, TRIMESTRAL, ANUAL;
    }

}
