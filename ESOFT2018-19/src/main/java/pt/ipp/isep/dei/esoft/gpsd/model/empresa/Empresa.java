/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pt.ipp.isep.dei.esoft.gpsd.model.empresa;

import pt.ipp.isep.dei.esoft.autorizacao.AutorizacaoFacade;
import pt.ipp.isep.dei.esoft.gpsd.model.AlgoritmoEscalonamento;
import pt.ipp.isep.dei.esoft.gpsd.model.CategoriaServico;
import pt.ipp.isep.dei.esoft.gpsd.model.Constantes;
import pt.ipp.isep.dei.esoft.gpsd.model.cliente.Cliente;

import java.util.*;

/**
 * @author Paulo Maio <pam@isep.ipp.pt>
 * @author Márcio Duarte
 * @author Luís Tavares
 */
public class Empresa {
    private final AutorizacaoFacade m_oAutorizacao;
    private final Set<Cliente> m_lstClientes;
    private final Set<CategoriaServico> m_lstCategoriaServicos;
    private final List<AlgoritmoEscalonamento> algoritmosEscalonamento;

    private final RegistoClientes registoClientes;
    private final RegistoServicos registoServicos;
    private final RegistoAreasGeograficas registoAreasGeograficas;
    private final RegistoPrestadoresServico registoPrestadoresServico;
    private final RegistoCategoriasServico registoCategoriasServico;
    private final RegistoPedidosPrestacaoServico registoPedidosPrestacaoServico;
    private final RegistoCandidaturasPrestadorServico registoCandidaturasPrestadorServico;
    private final RegistoAfetacoes registoAfetacoes;
	private final RegistoOrdensExecucaoServico registoOrdensExecucaoServico;
	private final RegistoExportadores registoExportadores;

    private final ServicoExterno servicoExterno;
	private ServicoExternoEmail servicoExternoEmail;

    private String m_strDesignacao;
    private String m_strNIF;

    private AfetarPrestadoresServicoTask task;
    private Timer timer;
    private long delay;
    private long intervalo;

    public Empresa(String strDesignacao, String strNIF) {
        if ((strDesignacao == null) || (strNIF == null) ||
                (strDesignacao.isEmpty()) || (strNIF.isEmpty()))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");

        this.m_strDesignacao = strDesignacao;
        this.m_strNIF = strNIF;

        this.m_oAutorizacao = new AutorizacaoFacade();

        this.m_lstClientes = new HashSet<>();
        this.m_lstCategoriaServicos = new HashSet<>();
        this.algoritmosEscalonamento = new ArrayList<>();
        this.registoServicos = new RegistoServicos(this);
        this.registoClientes = new RegistoClientes(this);
        this.registoAreasGeograficas = new RegistoAreasGeograficas();
        this.registoPrestadoresServico = new RegistoPrestadoresServico(this);
        this.registoCategoriasServico = new RegistoCategoriasServico();
        this.registoPedidosPrestacaoServico = new RegistoPedidosPrestacaoServico();
        this.registoCandidaturasPrestadorServico = new RegistoCandidaturasPrestadorServico();
        this.registoAfetacoes = new RegistoAfetacoes();
		this.registoOrdensExecucaoServico = new RegistoOrdensExecucaoServico();
		this.registoExportadores = new RegistoExportadores();

        this.servicoExterno = new ServicoExternoAreasGeograficas();

        initTasks();
    }

    private void initTasks() {
        this.task = new AfetarPrestadoresServicoTask();

		this.delay = Constantes.DEFAULT_DELAY;
		this.intervalo = Constantes.DEFAULT_INTERVALO;

        this.timer = new Timer();
        timer.schedule(task, delay, intervalo);
    }

    public AutorizacaoFacade getAutorizacaoFacade() {
        return this.m_oAutorizacao;
    }

    public RegistoServicos getRegistoServicos() {
        return registoServicos;
    }

    public RegistoClientes getRegistoClientes() {
        return registoClientes;
    }

    public RegistoCategoriasServico getRegistoCategorias() {
        return registoCategoriasServico;
    }

    public RegistoCandidaturasPrestadorServico getRegistoCandidaturasPrestadorServico() {
        return registoCandidaturasPrestadorServico;
    }

    // </editor-fold>

    // Categorias
    // <editor-fold defaultstate="collapsed">

    public CategoriaServico novaCategoria(String strCodigo, String strDescricao) {
        return new CategoriaServico(strCodigo, strDescricao);
    }

    public boolean registaCategoria(CategoriaServico oCategoriaServico) {
        if (this.validaCategoria(oCategoriaServico)) {
            return addCategoria(oCategoriaServico);
        }
        return false;
    }

    private boolean addCategoria(CategoriaServico oCategoriaServico) {
        return m_lstCategoriaServicos.add(oCategoriaServico);
    }

    public boolean validaCategoria(CategoriaServico oCategoriaServico) {
        boolean bRet = true;

        // Escrever aqui o código de validação

        //

        return bRet;
    }

    // </editor-fold>

    public AlgoritmoEscalonamento getAlgoritmoEscalonamento() {
        //Retorna um algoritmo de escalonamento aleatório visto não haver nenhumas diretrizes sobre qual escolher.
        return algoritmosEscalonamento.get((int)(Math.random() * (algoritmosEscalonamento.size() - 1)));
    }

    public RegistoAreasGeograficas getRegistoAreasGeograficas() {
        return registoAreasGeograficas;
    }

    public RegistoPrestadoresServico getRegistoPrestadoresServico() {
        return registoPrestadoresServico;
    }

    public RegistoCategoriasServico getRegistoCategoriasServico() {
        return registoCategoriasServico;
    }

    public RegistoPedidosPrestacaoServico getRegistoPedidosPrestacaoServico() {
        return registoPedidosPrestacaoServico;
    }

    public RegistoAfetacoes getRegistoAfetacoes() {
        return registoAfetacoes;
    }

	public RegistoOrdensExecucaoServico getRegistoOrdensExecucaoServico() {
		return registoOrdensExecucaoServico;
	}

    public ServicoExterno getServicoExterno() {
        return servicoExterno;
    }

	public ServicoExternoEmail getServicoExternoEmail() {
		return servicoExternoEmail;
	}

    public RegistoExportadores getRegistoExportadores() {
        return registoExportadores;
    }
}
    
    
