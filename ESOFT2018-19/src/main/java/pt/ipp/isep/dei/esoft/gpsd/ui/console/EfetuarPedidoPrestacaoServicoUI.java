package pt.ipp.isep.dei.esoft.gpsd.ui.console;

import pt.ipp.isep.dei.esoft.gpsd.controller.EfetuarPedidoPrestacaoServicoController;
import pt.ipp.isep.dei.esoft.gpsd.model.CategoriaServico;
import pt.ipp.isep.dei.esoft.gpsd.model.EnderecoPostal;
import pt.ipp.isep.dei.esoft.gpsd.model.servico.Servico;
import pt.ipp.isep.dei.esoft.gpsd.ui.console.utils.Utils;

import java.util.List;

/**
 * UI do caso de uso 6, efetução do pedido de prestação de serviços.
 *
 * @author Márcio Duarte
 * @author Luís Tavares
 */
public class EfetuarPedidoPrestacaoServicoUI {

	/**
	 * Controller do UC6.
	 */
	private EfetuarPedidoPrestacaoServicoController controller;

	/**
	 * Constructor.
	 */
	public EfetuarPedidoPrestacaoServicoUI() {
		this.controller = new EfetuarPedidoPrestacaoServicoController();
	}

	/**
	 * Método que deve correr aquando da efetuação do pedido de prestação de serviços.
	 */
	public void run() {
		do {
			do {
				criarPedido();
				addServicos();
				addPreferenciaHorarios();
			} while (!controller.valida());

			System.out.println(controller.getPedidoPrestacaoServico());
		} while (!Utils.confirma("Confirma? "));

		int numero = controller.registaPedido();

		System.out.println("O pedido ficou registado com o número " + numero);
	}

	/**
	 * Cria pedido e inicializa o UC.
	 */
	private void criarPedido() {
		controller.inicializarDados();

		EnderecoPostal enderecoPostal = getEnderecoPostal();
		controller.criarPedido(enderecoPostal);
	}

	/**
	 * Adiciona serviços ao pedido de prestação de serviços.
	 */
	private void addServicos() {
		boolean valido;
		List<CategoriaServico> listaCategorias = controller.getCategorias();
		do {
			do {
				String categoriaID = ((CategoriaServico) Utils.apresentaESeleciona(listaCategorias, "Selecione uma categorias")).getCodigo();
				List<Servico> listaServicos = controller.getServicosDeCategoria(categoriaID);
				Servico servico = (Servico) Utils.apresentaESeleciona(listaServicos, "Selecione o serviço. ");
				String descricao = Utils.readLineFromConsole("Descrição do serviço? ");
				String duracao = Utils.readLineFromConsole("Duração do serviço? (deverá ser múltiplo de 30) ");
				valido = controller.addPedidoServico(servico, descricao, duracao);
			} while (!valido);
		} while (Utils.confirma("Deseja adicionar mais serviços? "));
	}

	/**
	 * Encontra o endereço postal que o utilizador pretende.
	 *
	 * @return um endereço postal.
	 */
	private EnderecoPostal getEnderecoPostal() {
		List<EnderecoPostal> listaEnderecosPostais = controller.getEnderecosPostais();

		EnderecoPostal enderecoPostal;
		do {
			Utils.apresentaLista(listaEnderecosPostais, "Escolha um endereço postal. ");
			enderecoPostal = selecionaEnderecoPostal(listaEnderecosPostais);
		} while (enderecoPostal == null);
		return enderecoPostal;
	}

	/**
	 * Adiciona preferencia de horários ao pedido de prestação de serviços.
	 */
	private void addPreferenciaHorarios() {
		do {
			boolean valido;
			do {
				String strData = Utils.readLineFromConsole("Data? ");
				String strHora = Utils.readLineFromConsole("Hora? ");
				valido = controller.addHorario(strData, strHora);
			} while (!valido);
		} while (Utils.confirma("Deseja adicionar mais preferências? "));
	}

	/**
	 * Permite a seleçao de um endereço postal a partir de uma lista.
	 *
	 * @param list lista de endereços postais.
	 * @return o endereço postal escolhido ou null caso o escolhido seja inválido.
	 */
	private EnderecoPostal selecionaEnderecoPostal(List list) {
		System.out.println((list.size() + 1) + ". Adicionar outro endereço postal");
		String opcao;
		int nOpcao;

		do {
			opcao = Utils.readLineFromConsole("Introduza opção: ");
			nOpcao = new Integer(opcao);
		} while (nOpcao < 0 || nOpcao > list.size() + 1);

		if (nOpcao == list.size() + 1) {
			AssociarEnderecoPostalAClienteUI associarEnderecoPostalAClienteUI = new AssociarEnderecoPostalAClienteUI();
			associarEnderecoPostalAClienteUI.run();
			this.run();
		}
		if (nOpcao == 0) {
			return null;
		} else {
			return (EnderecoPostal) list.get(nOpcao - 1);
		}
	}
}
