package pt.ipp.isep.dei.esoft.gpsd.model;

/**
 * Custos adicionais que alberga o tipo de custo e o custo.
 */
public class OutroCusto {

	/**
	 * Tipo de custo (e.g. "deslocação").
	 */
	private String tipoDeCusto;

	/**
	 * Custo em float.
	 */
	private float custo;

	/**
	 * Construtor que recebe todos os parametros.
	 *
	 * @param tipoDeCusto Tipo de custo (e.g. "deslocação").
	 * @param custo       Custo em float.
	 */
	public OutroCusto(String tipoDeCusto, float custo) {
		this.tipoDeCusto = tipoDeCusto;
		this.custo = custo;
	}

	/**
	 * Retorna o custo.
	 *
	 * @return o custo.
	 */
	public float getCusto() {
		return custo;
	}
}
