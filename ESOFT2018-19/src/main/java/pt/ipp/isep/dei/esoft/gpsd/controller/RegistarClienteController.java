/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.controller;

import pt.ipp.isep.dei.esoft.gpsd.model.EnderecoPostal;
import pt.ipp.isep.dei.esoft.gpsd.model.cliente.Cliente;
import pt.ipp.isep.dei.esoft.gpsd.model.cliente.ListaEnderecosPostais;
import pt.ipp.isep.dei.esoft.gpsd.model.empresa.Empresa;
import pt.ipp.isep.dei.esoft.gpsd.model.empresa.RegistoClientes;
import pt.ipp.isep.dei.esoft.gpsd.ui.console.utils.Utils;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author paulomaio
 */
public class RegistarClienteController {

	private AplicacaoGPSD m_oApp;
	private Empresa m_oEmpresa;
	private Cliente m_oCliente;
	private String m_strPwd;
	private RegistoClientes registoClientes;

	public RegistarClienteController() {
		this.m_oApp = AplicacaoGPSD.getInstance();
		this.m_oEmpresa = m_oApp.getEmpresa();
	}


	public boolean novoCliente(String strNome, String strNIF, String strTelefone, String strEmail, String strPwd, String strLocal, String strCodPostal, String strLocalidade) {
		try {
			m_strPwd = strPwd;

			EnderecoPostal morada = ListaEnderecosPostais.novoEnderecoPostal(strLocal, strCodPostal, strLocalidade);
			registoClientes = m_oEmpresa.getRegistoClientes();

			m_oCliente = registoClientes.novoCliente(strNome, strNIF, strTelefone, strEmail, morada);
			return registoClientes.validaCliente(this.m_oCliente, this.m_strPwd);

		} catch (RuntimeException ex) {
			Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
			this.m_oCliente = null;
			return false;
		}
	}

	public boolean addEnderecoPostal(String strLocal, String strCodPostal, String strLocalidade) {
		if (this.m_oCliente != null) {
			try {
				return m_oCliente.adicionarEnderecoPostal(strLocal, strCodPostal, strLocalidade);
			} catch (RuntimeException ex) {
				Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
				return false;
			}
		}
		return false;
	}

	public boolean registaCliente() {
		return this.registoClientes.registaCliente(this.m_oCliente, this.m_strPwd);
	}

	public String getClienteString() {
		return this.m_oCliente.toString();
	}
}
