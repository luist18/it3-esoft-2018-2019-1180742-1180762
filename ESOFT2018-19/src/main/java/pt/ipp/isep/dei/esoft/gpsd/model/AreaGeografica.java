package pt.ipp.isep.dei.esoft.gpsd.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Representação de uma área geográfica.
 *
 * @author Márcio Duarte
 * @author Luís Tavares
 */
public class AreaGeografica {

    /**
     * Designação ou nome da área geográfica.
     */
    private String designacao;

    /**
     * Custo da área geográfica.
     */
    private double custo;

    /**
     * Código postal onde a área geográfica se baseia.
     */
    private CodigoPostal codigoPostalBase;

    /**
     * Raio da área geofráfica.
     */
    private double raio;

	private String id;

    /**
     * Lista de atuação da área geográfica.
     */
	private List<Atuacao> listaAtuacao = new ArrayList<>();

	public AreaGeografica(String designacao, double custo, CodigoPostal codigoPostalBase, double raio, String id) {
		this.designacao = designacao;
		this.custo = custo;
		this.codigoPostalBase = codigoPostalBase;
		this.raio = raio;
		this.id = id;
	}

    /**
     * Construtor de uma área geográfica que recebe todos os parametros.
     *
     * @param designacao       designação ou nome da área geográfica.
     * @param custo            custo da área geográfica.
     * @param codigoPostalBase codigo postal base da área geográfica.
     * @param raio             raio da área geográfica.
     */
    public AreaGeografica(String designacao, double custo, CodigoPostal codigoPostalBase, double raio, List<Atuacao> listaAtuacao) {
        this.designacao = designacao;
        this.custo = custo;
        this.codigoPostalBase = codigoPostalBase;
        this.raio = raio;
        this.listaAtuacao = listaAtuacao;
    }

    /**
     * toString com todas as informações sobre a área geográfica.
     *
     * @return uma string com todas as informações sobre a área geográfica.
     */
    @Override
    public String toString() {
        return String.format("%s, centrado em %s", designacao, codigoPostalBase);
    }


    /**
     * Retorna a lista de atuação da área geográfica.
     *
     * @return a lista de atuação da área geográfica.
     */
    public List<Atuacao> getListaAtuacao() {
        return listaAtuacao;
    }

    /**
     * Valida o seus dados.
     *
     * @return se os dados estão corretos.
     */
    public boolean validar() {
        return false;
    }

	public String getID() {
		return id;
	}
}
