package pt.ipp.isep.dei.esoft.gpsd.model.empresa;

import pt.ipp.isep.dei.esoft.gpsd.model.CandidaturaPrestadorServico;

import java.util.ArrayList;
import java.util.List;

/**
 * Registo que guarda todas as candidaturas a prestador de serviço.
 *
 * @author Márcio Duarte
 * @author Luís Tavares
 */
public class RegistoCandidaturasPrestadorServico {

	/**
	 * Lista com todas as candidaturas.
	 */
	private List<CandidaturaPrestadorServico> candidaturas = new ArrayList<>();

	/**
	 * Construtor.
	 */
	public RegistoCandidaturasPrestadorServico() {
	}

	/**
	 * Verifica se existe candidatura para um certo NIF.
	 *
	 * @param nif NIF a verificar.
	 * @return se existe a candidatura.
	 */
	public boolean existeCandidatura(String nif) {
		return getCandidatura(nif) != null;
	}

	/**
	 * Encontra uma candidatura com um certo NIF.
	 * Retorna null se nao existir tal candidatura.
	 *
	 * @param nif NIF para pesquisar a candidatura.
	 * @return a candidatura encontrada ou null.
	 */
	public CandidaturaPrestadorServico getCandidatura(String nif) {
		for (CandidaturaPrestadorServico candidatura : candidaturas)
			if (candidatura.hasNif(nif))
				return candidatura;

		return null;
	}
}
