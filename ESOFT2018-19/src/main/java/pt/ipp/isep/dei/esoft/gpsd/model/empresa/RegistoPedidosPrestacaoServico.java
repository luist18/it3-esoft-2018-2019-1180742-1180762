package pt.ipp.isep.dei.esoft.gpsd.model.empresa;

import pt.ipp.isep.dei.esoft.gpsd.model.EnderecoPostal;
import pt.ipp.isep.dei.esoft.gpsd.model.PedidoPrestacaoServico;
import pt.ipp.isep.dei.esoft.gpsd.model.cliente.Cliente;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Classe que controla pedidos de prestação de serviço.
 *
 * @author Márcio Duarte
 * @author Luís Tavares
 */
public class RegistoPedidosPrestacaoServico {

	/**
	 * Lista com todos os pedidos de prestação de serviços
	 */
	private List<PedidoPrestacaoServico> listaPedidos = new ArrayList<>();

	/**
	 * Construtor.
	 */
	public RegistoPedidosPrestacaoServico() {

	}

	/**
	 * Cria um pedido de prestacao de serviços.
	 *
	 * @param cliente        cliente para quem se dirige o pedido.
	 * @param enderecoPostal endereço postal para a prestção de serviços.
	 * @return o pedido criado.
	 */
	public PedidoPrestacaoServico novoPedido(Cliente cliente, EnderecoPostal enderecoPostal) {
		return new PedidoPrestacaoServico(cliente, enderecoPostal);
	}

	/**
	 * Calcula o custo do pedido e verifica-o.
	 *
	 * @param pedido pedido a terminar.
	 * @return o pedido é válido.
	 */
	public boolean validarPedido(PedidoPrestacaoServico pedido) {
		pedido.calcularCustoTotal();
		return verificarPedido(pedido);
	}

	/**
	 * Regista um pedido de prestação de serviços.
	 *
	 * @param pedido pedido a registar.
	 * @return o número com o qual o pedido ficou registado.
	 */
	public int registarPedido(PedidoPrestacaoServico pedido) {
		int numero = geraNumeroPedido();
		pedido.setNumero(numero);
		listaPedidos.add(pedido);

		pedido.definirSubmetido();

		return numero;
	}

	/**
	 * Retorna um set com todos os pedidos submetidos.
	 *
	 * @return um set com todos os pedidos submetidos.
	 */
	public List<PedidoPrestacaoServico> getPedidosSubmetidos() {
		List<PedidoPrestacaoServico> pedidosSubmetidos = new ArrayList<>();

		for (PedidoPrestacaoServico pedido : listaPedidos)
			if (pedido.isSubmetido())
				pedidosSubmetidos.add(pedido);

		return pedidosSubmetidos;
	}

	/**
	 * Verifica se um pedido é válido.
	 *
	 * @param pedido pedido a validar.
	 * @return se o pedido é válido.
	 */
	private boolean verificarPedido(PedidoPrestacaoServico pedido) {
		return listaPedidos.contains(pedido);
	}

	/**
	 * Gera número para pedido.
	 *
	 * @return o numero gerado.
	 */
	private int geraNumeroPedido() {
		return listaPedidos.size() + 1;
	}
}
