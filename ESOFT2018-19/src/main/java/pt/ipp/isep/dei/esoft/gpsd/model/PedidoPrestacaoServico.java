package pt.ipp.isep.dei.esoft.gpsd.model;

import pt.ipp.isep.dei.esoft.gpsd.model.cliente.Cliente;
import pt.ipp.isep.dei.esoft.gpsd.model.empresa.RegistoAfetacoes;
import pt.ipp.isep.dei.esoft.gpsd.model.servico.Servico;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Pedido para que seja feita a prestação de serviços.
 */
public class PedidoPrestacaoServico {

	/**
	 * Cliente para quem o pedido será efetuado.
	 */
	private Cliente cliente;

	/**
	 * Endereço postal onde o serviço será efetuado.
	 */
	private EnderecoPostal enderecoPostal;

	/**
	 * Custo do pedido de prestação.
	 */
	private float custo;

	/**
	 * Número como que ID.
	 */
	private int numero;

	/**
	 * Estado do pedido.
	 */
	private EstadoPedido estado;

	/**
	 * Lista dos serviços que fazem parte do pedido.
	 */
	private List<DescricaoServico> listaServicosPedidos = new ArrayList<>();

	/**
	 * Lista com os horários e a sua preferencia.
	 */
	private List<PreferenciaHorario> listaPreferenciaHorarios = new ArrayList<>();

	/**
	 * Construtor com todos os parametros.
	 *
	 * @param cliente        Cliente para quem o pedido será efetuado.
	 * @param enderecoPostal Endereço postal onde o serviço será efetuado.
	 */
	public PedidoPrestacaoServico(Cliente cliente, EnderecoPostal enderecoPostal) {
		this.cliente = cliente;
		this.enderecoPostal = enderecoPostal;
	}

	/**
	 * Adiciona um serviço ao pedido de prestação.
	 *
	 * @param servico    serviço a adicionar.
	 * @param descricao  descrição do serviço.
	 * @param strDuracao duração do serviço.
	 * @return se o pedido foi corretamente adicionado.
	 */
	public boolean addPedidoServico(Servico servico, String descricao, String strDuracao) {
		DescricaoServico descricaoServico = new DescricaoServico(servico, descricao, strDuracao);
		if (!descricaoServico.validarPedidoServico()) return false;
		return listaServicosPedidos.add(descricaoServico);
	}

	/**
	 * Adiciona um horário à lista de preferencias.
	 *
	 * @param strData data do horário.
	 * @param strHora hora do horário.
	 * @return se o horário foi devidamente adicionado.
	 */
	public boolean addHorario(String strData, String strHora) {
		int ordem = countHorarios();
		PreferenciaHorario preferenciaHorario = new PreferenciaHorario(strData, strHora, ordem);

		if (validaHorario(preferenciaHorario))
			return listaPreferenciaHorarios.add(preferenciaHorario);
		else
			return false;
	}

	/**
	 * Calcula o custo total de um pedido de prestação de serviços.
	 */
	public void calcularCustoTotal() {
		float custo = 0;

		custo += geraCustoServicos();

		float custoDeslocacao = calcularCustoDeslocação();
		OutroCusto custoAdicional = new OutroCusto("deslocaçao", custoDeslocacao);

		custo += custoAdicional.getCusto();

		this.custo = custo;
	}

	/**
	 * Coloca um pedido como submetido.
	 */
	public void definirSubmetido() {
		estado = EstadoPedido.SUBMETIDO;
	}

	/**
	 * Verifica se um pedido está submetido.
	 *
	 * @return se o pedido está submetido ou não.
	 */
	public boolean isSubmetido() {
		return estado.equals(EstadoPedido.SUBMETIDO);
	}

	/**
	 * Compara o pedido de prestação de serviços a outro.
	 *
	 * @param o objeto a comparar com este pedido.
	 * @return se o pedido é o mesmo.
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof PedidoPrestacaoServico)) return false;
		PedidoPrestacaoServico that = (PedidoPrestacaoServico) o;
		return Float.compare(that.custo, custo) == 0 &&
				Objects.equals(cliente, that.cliente) &&
				Objects.equals(enderecoPostal, that.enderecoPostal) &&
				Objects.equals(listaServicosPedidos, that.listaServicosPedidos) &&
				Objects.equals(listaPreferenciaHorarios, that.listaPreferenciaHorarios);
	}

	@Override
	public String toString() {
		return "PedidoPrestacaoServico{" +
				"cliente=" + cliente +
				", enderecoPostal=" + enderecoPostal +
				", custo=" + custo +
				'}';
	}

	public Cliente getCliente() {
		return cliente;
	}

	/**
	 * Gera custos adicionais para o pedido.
	 *
	 * @return o custo adicional.
	 */
	private float geraCustoServicos() {
		float custo = 0;
		for (DescricaoServico descricaoServico : listaServicosPedidos)
			custo += descricaoServico.getServico().getCusto();

		return custo;
	}

	private float calcularCustoDeslocação() {
		//TODO: implement method.
		return 0;
	}

	/**
	 * Define número de pedido.
	 *
	 * @param numero novo número do pedido.
	 */
	public void setNumero(int numero) {
		this.numero = numero;
	}

	/**
	 * Verifica se todos os serviços do pedido foram afetados.
	 *
	 * @param registoAfetacoes registo de afetações para se verificar.
	 * @return se todos os serviços foram afetados.
	 */
	public boolean verificarSeServicosForamAfetados(RegistoAfetacoes registoAfetacoes) {
		for (DescricaoServico descricaoServico : listaServicosPedidos)
			if (!registoAfetacoes.verificarSeServicoEstaAfetado(descricaoServico))
				return false;

		return true;
	}

	/**
	 * Getter da lista de descriçoes de serviço.
	 *
	 * @return lista de descriçoes de serviço.
	 */
	public List<DescricaoServico> getListaDescicoesServico() {
		return listaServicosPedidos;
	}

	/**
	 * Valida um horário.
	 *
	 * @param preferenciaHorario horário a validar.
	 * @return se o horário é válido.
	 */
	private boolean validaHorario(PreferenciaHorario preferenciaHorario) {
		//TODO: add validações
		return !listaPreferenciaHorarios.contains(preferenciaHorario);
	}

	/**
	 * Conta o número de horários.
	 *
	 * @return o número de horários.s
	 */
	private int countHorarios() {
		return listaPreferenciaHorarios.size();
	}

	/**
	 * Estado do pedido.
	 */
	private enum EstadoPedido {
		SUBMETIDO, REALIZADO
	}
}
