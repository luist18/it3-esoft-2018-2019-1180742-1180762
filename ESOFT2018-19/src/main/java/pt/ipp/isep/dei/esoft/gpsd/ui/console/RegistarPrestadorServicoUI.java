package pt.ipp.isep.dei.esoft.gpsd.ui.console;

import pt.ipp.isep.dei.esoft.gpsd.controller.RegistarPrestadorServicoController;
import pt.ipp.isep.dei.esoft.gpsd.model.AreaGeografica;
import pt.ipp.isep.dei.esoft.gpsd.model.CategoriaServico;
import pt.ipp.isep.dei.esoft.gpsd.ui.console.utils.Utils;

import java.util.List;

/**
 * Classe responsável pela UI de registo de prestador de serviço.
 *
 * @author Márcio Duarte
 * @author Luís Tavares
 */
public class RegistarPrestadorServicoUI {

	/**
	 * Controller do UC de registo de prestador de serviço.
	 */
	private RegistarPrestadorServicoController controller;

	/**
	 * Construtor da UI que inicializa o controller.
	 */
	public RegistarPrestadorServicoUI() {
		controller = new RegistarPrestadorServicoController();
	}

	/**
	 * Corre a UI de registo de prestador de serviço.
	 */
	public void run() {
		do {
			if (localizarCandidaturaPrestador()) {
				criarPrestadorAPartirDeCandidatura();
			} else {
				criarPrestadorComDadosIniciais();
				if (!adicionarAreasGeograficas()) return;
				if (!adicionarCategoriasServico()) return;
			}
		} while (!mostrarDadosEConfirmar());

		controller.registarPrestadorServico();

		System.out.println("\nPrestador registado com sucesso!\n");
	}

	/**
	 * Verifica se exsite candidatura para o presrador que o utilizador deseja registar.
	 *
	 * @return se existe ou não
	 */
	private boolean localizarCandidaturaPrestador() {
		String nif = Utils.readLineFromConsole("NIF do prestador?");
		boolean existeCandidaturas = controller.verificarCandidaturas(nif);

		if (existeCandidaturas) {
			System.out.println("Foi encontrada uma candidatura!");
			System.out.println("Nome Completo: " + controller.getCandidatura().getNomeCompleto());
			Utils.mostraLista(controller.getCandidatura().getAreasGeograficas(), "Áreas Geográficas: ");
			Utils.mostraLista(controller.getCandidatura().getCategorias(), "Categorias: ");

			return Utils.confirma("Deseja adicionar este prestador?");
		} else {
			return false;
		}
	}

	/**
	 * Cria o presador de serviço com os dados iniciais (i.e. numero, nome e email).
	 */
	private void criarPrestadorComDadosIniciais() {
		boolean valido;
		do {
			String strNumero = Utils.readLineFromConsole("Número mecanográfico? ");
			String nomeComp = Utils.readLineFromConsole("Nome completo? ");
			String nomeAbrev = Utils.readLineFromConsole("Nome abreviado? ");
			String email = Utils.readLineFromConsole("Email institucional? ");

			controller.criarPrestadorServico(strNumero, nomeComp, nomeAbrev, email);
			valido = controller.validarPrestadorServico();

			if (!valido)
				System.out.println("\nCredenciais inválidas, tente outra vez!");
		} while (!valido);
	}

	/**
	 * Cria prestador a partir dos dados da candidatura correspondente.
	 */
	private void criarPrestadorAPartirDeCandidatura() {
		do {
			String strNumero = Utils.readLineFromConsole("Número mecanográfico? ");
			String nomeComp = controller.getCandidatura().getNomeCompleto();
			String nomeAbrev = Utils.readLineFromConsole("Nome abreviado? ");
			String email = Utils.readLineFromConsole("Email institucional? ");

			controller.criarPrestadorServico(strNumero, nomeComp, nomeAbrev, email);
			controller.carregarListasCandidatura();
		} while (!controller.validarPrestadorServico());
	}

	/**
	 * Adiciona áreas geográficas ao prestador enquanto o utilizador desejar adicionar mais.
	 *
	 * @return se foi adicionada área.
	 */
	private boolean adicionarAreasGeograficas() {
		boolean valido;

		do {
			valido = true;
			List<AreaGeografica> areasGeograficas = controller.getAreasGeograficas();

			AreaGeografica chosen = (AreaGeografica) Utils.apresentaESeleciona(areasGeograficas, "\nEscolha uma área geográfica. ");

			if (chosen == null)
				break;

			String id = chosen.getID();
			if (!controller.addAreaGeografica(id)) {
				valido = false;
				System.out.println("Essa categoria não é válida tente de novo. ");
			}
		} while (!valido || Utils.confirma("Deseja adicionar mais áreas geográficas? "));

		return !controller.getPrestadorServico().getAreasGeograficas().isEmpty();
	}

	/**
	 * Adiciona categorias de serviço ao prestador.
	 *
	 * @return se as categorias de serviço foram devidamente adicionadas.
	 */
	private boolean adicionarCategoriasServico() {
		boolean valido;

		List<CategoriaServico> categorias = controller.getCategoriasServico();

		do {
			valido = true;

			CategoriaServico chosen = (CategoriaServico) Utils.apresentaESeleciona(categorias, "\nEscolha categoria. ");

			if (chosen == null)
				break;

			String id = chosen.getCodigo();

			if (!controller.addCategoria(id)) {
				System.out.println("\nCategoria inválida!\n");
				valido = false;
			}

		} while (!valido || Utils.confirma("Deseja adicionar mais categorias? "));

		return !controller.getPrestadorServico().getCategoriasServico().isEmpty();
	}

	/**
	 * Mostra os dados introduzidos e solicita a confirmação.
	 *
	 * @return se o utilizador confirma ou não os dados.
	 */
	private boolean mostrarDadosEConfirmar() {
		System.out.println(controller.getPrestadorServico());
		return Utils.confirma("Confirma? ");
	}
}
