package pt.ipp.isep.dei.esoft.gpsd.model;

import pt.ipp.isep.dei.esoft.gpsd.model.prestador.PrestadorServico;

import java.util.Date;

/**
 * Ordem de execução de serviço está associada aos prestadores fazendo com que estes conheçam os
 * serviços que estão propostos a realizar.
 */
public class OrdemExecucaoServico {

    /**
     * Prestador associado à ordem de execução.
     */
    private PrestadorServico prestadorServico;

    /**
     * Pedido de prestação de serviço associado à prestação.
     */
    private PedidoPrestacaoServico pedidoPrestacaoServico;

    /**
     * Descição de serviço realilzada pelo prestador.
     */
    private DescricaoServico descricaoServico;

    /**
     * Horário da ordem de execução de serviço.
     */
    private Date horario;

    /**
     * Estado da ordem de execucao de serviço.
     */
    private String estado;

    /**
     * Contrutor que recebe todos os elementos.
     *
     * @param prestadorServico       prestador associado à ordem de execução.
     * @param pedidoPrestacaoServico pedido de prestação de serviço associado à prestação.
     * @param descricaoServico       descição de serviço realilzada pelo prestador.
     * @param horario                horário da ordem de execução de serviço.
     * @param estado                 estado da ordem de execução de serviço.
     */
    public OrdemExecucaoServico(PrestadorServico prestadorServico, PedidoPrestacaoServico pedidoPrestacaoServico, DescricaoServico descricaoServico, Date horario, String estado) {
        this.prestadorServico = prestadorServico;
        this.estado = estado;
        this.pedidoPrestacaoServico = pedidoPrestacaoServico;
        this.descricaoServico = descricaoServico;
        this.horario = horario;
    }

    /**
     * Retorna o estado da ordem de execução de serviço.
     *
     * @return o estado.
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Define o estado da ordem de execução de serviço.
     *
     * @param estado o estado.
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * Retorna o prestador da ordem de execução de serviço.
     *
     * @return o prestador.
     */
    public PrestadorServico getPrestadorServico() {
        return prestadorServico;
    }
}
