package pt.ipp.isep.dei.esoft.gpsd.model.empresa;

import pt.ipp.isep.dei.esoft.gpsd.model.CategoriaServico;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe responsável pela gestão das categorias de serviço.
 *
 * @author Márcio Duarte
 * @author Luís Tavares
 */
public class RegistoCategoriasServico {

	/**
	 * Lista com todas as categorias de serviço
	 */
	private List<CategoriaServico> categoriasServico = new ArrayList<>();

	/**
	 * Construtor do registo.
	 */
	public RegistoCategoriasServico() {
		//TODO: complete
	}

	/**
	 * Encontra uma categoria pelo seu ID e retorna-a.
	 *
	 * @param strCategoriaId id da categoria a encontrar.
	 * @return a categoria correspondente ao ID enviado.
	 */
	public CategoriaServico getCategoriaById(String strCategoriaId) {
		for (CategoriaServico cat : this.categoriasServico) {
			if (cat.hasId(strCategoriaId)) {
				return cat;
			}
		}
		return null;
	}

	/**
	 * Cria uma nova categoria de serviço.
	 *
	 * @param strCodigo    codigo da categoria.
	 * @param strDescricao descrição da categoria.
	 * @return a categoria de serviço criada.
	 */
	public CategoriaServico novaCategoria(String strCodigo, String strDescricao) {
		return new CategoriaServico(strCodigo, strDescricao);
	}

	/**
	 * Valida uma categoria de serviço.
	 * @param oCategoria categoria a ser validada.
	 * @return se a categoria é válida ou nao.
	 */
	public boolean validaCategoria(CategoriaServico oCategoria) {
		boolean bRet = true;

		// Escrever aqui o código de validação

		return bRet;
	}

	/**
	 * Regista uma categoria de serviço.
	 * @param oCategoria categoria a ser registada.
	 * @return se a categoria foi devidamente registada.
	 */
	public boolean registaCategoria(CategoriaServico oCategoria) {
		if (this.validaCategoria(oCategoria)) {
			return addCategoria(oCategoria);
		}
		return false;
	}

	/**
	 * Adiciona uma categoria de serviço.
	 * @param oCategoria categoria a ser adicionada.
	 * @return se a adição foi cumprida.
	 */
	private boolean addCategoria(CategoriaServico oCategoria) {
		return categoriasServico.add(oCategoria);
	}

	/**
	 * Retorna uma lista com todas as categorias de serviço.
	 *
	 * @return uma lista com todas as categorias de serviço.
	 */
	public List<CategoriaServico> getCategoriasServico() {
		return new ArrayList<>(this.categoriasServico);
	}
}
