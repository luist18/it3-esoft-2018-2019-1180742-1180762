package pt.ipp.isep.dei.esoft.gpsd.model.empresa;

import pt.ipp.isep.dei.esoft.gpsd.model.CategoriaServico;
import pt.ipp.isep.dei.esoft.gpsd.model.servico.*;

import java.util.ArrayList;
import java.util.List;

public class RegistoServicos {

    private Empresa empresa;

    private List<Servico> servicos = new ArrayList<>();

    public RegistoServicos(Empresa empresa) {
        this.empresa = empresa;
    }

    public Servico novoServico(String id, String descB, String descC, double custo, CategoriaServico categoriaServico, TipoServico ts){
        switch (ts){
            case FIXO:
                return new ServicoFixo(id, descB, descC, custo, categoriaServico);
            case LIMITADO:
                return new ServicoLimitado(id, descB, descC, custo, categoriaServico);
            case EXPANSIVEL:
                return new ServicoExpansivel(id, descB, descC, custo, categoriaServico);
            default:
                return null;
        }
    }

    public List<Servico> getServicosDeCategoria(String categoriaID) {
        List<Servico> servicosRequeridos = new ArrayList<>(this.servicos.size());

        for(Servico servico : servicos)
            if (servico.getCategoria().getCodigo().equals(categoriaID))
                servicosRequeridos.add(servico);


        return servicosRequeridos;
    }

    public boolean validaServico(Servico servico) {
        return verificaServico(servico) && servico.validaServico();
    }

    private boolean verificaServico(Servico servico) {
        return !servicos.contains(servico);
    }

    public boolean registaServico(Servico serv) {
        return servicos.add(serv);
    }
}
