package pt.ipp.isep.dei.esoft.gpsd.model.servico;

import pt.ipp.isep.dei.esoft.gpsd.model.CategoriaServico;

public interface Servico {

    boolean possuiOutrosAtributos();

    double getOutrosAtributos();

    void setOutrosAtributos(double atrib);

    boolean validaServico();

    double getCusto();

    CategoriaServico getCategoria();
}
