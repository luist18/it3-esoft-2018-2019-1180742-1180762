package pt.ipp.isep.dei.esoft.gpsd.model;

import pt.ipp.isep.dei.esoft.gpsd.model.empresa.RegistoAfetacoes;
import pt.ipp.isep.dei.esoft.gpsd.model.prestador.PrestadorServico;

import java.util.List;

/**
 * Algoritmo capaz de afetar pedidos de prestação aos prestadores de serviço.
 *
 * @author Márcio Duarte
 * @author Luís Tavares
 */
public interface AlgoritmoEscalonamento {

	/**
	 * Afeta os prestadores de serviço aos pedidos.
	 * O algoritmo deve ser capaz de criar de as afetações.
	 *
	 * @param listaPedidosPrestacao lista de pedidos de prestacao de serviço.
	 * @param listaPrestadoresServico lista de prestadores de serviço.
	 * @param registoAfetacoes registo de afetações para que sejam criadas as afetações.
	 */
	void afetarPrestadoresServico(List<PedidoPrestacaoServico> listaPedidosPrestacao,
								  List<PrestadorServico> listaPrestadoresServico,
								  RegistoAfetacoes registoAfetacoes);
}
